!-----------------------------------------------------------------
!
! Simple test program for the mpidomain class (1D-2D domain decomp)
! and hdf5 I/O
! authors: Markus Rampp, Fabio Baruffa (RZG) 
!-----------------------------------------------------------------

program main
use precision
#ifdef MPICODE
use mpi
#endif
use my_mpi
use mod_mpidomain
!use mod_hdf5io
implicit none

!problem setup
integer, parameter :: hsize=1,xsize=4,ysize=16,zsize=20  ! global grid dimensions
integer, parameter :: gs=1                              ! number of ghost zones
integer, parameter :: nstep=1                           ! number of time steps
integer, parameter :: domain_rank=2		        ! rank of the domain decomposition

real(kind=PREC_F90), allocatable :: f(:,:,:),g(:,:,:,:)		 ! local array for each MPI rank
real(kind=PREC_F90) :: f_global(1:xsize,1:ysize,1:zsize)         ! global array over the entire grid dimension
real(kind=PREC_F90) :: g_global(1:hsize,1:xsize,1:ysize,1:zsize) ! global array over the entire grid dimension

real(kind=PREC_F90) :: scale
real(kind=PREC_F90) :: arr_1d(10)
integer :: n

character(len=*),parameter :: file_name='testfile' !name of the file to be written in h5 format
integer :: source, dest, rank
!MPI init
call initmpi(ierr)

!domain init
domain=mpidomain(domain_rank,xsize,ysize,zsize,gs,ierr)

!produce some informational output for the domain
call domain%infoDomain()
call domain%info()

! allocate a 3D array with halo cells
call domain%alloc(f,withhalos=.true.)
! allocate a 4D array with leading dimensions 1:hsize and halo cells
call domain%alloc(g,1,hsize,withhalos=.true.) ! allocate a 4D array
  f(:,:,:)= 1._rk                  !initialize field
g(:,:,:,:)= 1._rk

! call initializeFunction		!initialize internal points with rank number

call MPI_CART_SHIFT(domain%mpi_comm_cart, 1 , 1, source, dest, ierr)
call MPI_COMM_RANK(domain%mpi_comm_cart,rank,ierr)
write(*,*), rank, source, dest


!HDF5 I/O init
!call h5file%initialize()

!create the hdf5 file
!call h5file%create_file(file_name)

!write basic setup parameters 
!call h5file%create_group('trial_G')
!call h5file%close_group('trial_G')

scale=0.45_rk
call updateField(scale)  

!call h5file%create_group('function_F')
!call h5file%write_lc(f,'f_0')
!emulate a timestepping loop with f and g

call printFlocal
do n=1,nstep
   scale=1.0_PREC_F90/real(1)
   scale=0.5_rk
   call domain%updateHalos(f)   	!update halos
   !call domain%updateHalos(g,1,hsize)   !update halos
   call updateField(scale)      	!update field
   if (comm%isRoot()) write(*,*) 'Time step: ',n, ' scale: ', scale**n
enddo
call printFlocal

!call h5file%write_lc(f,'f_1')
!call h5file%write_lc(g,1,hsize,'g')
!  
!call h5file%close_group('function_F')
! 
!call domain%updateGlobal(f,f_global)
!call domain%updateGlobal(g,1,hsize,g_global)
! call printFGlobal
! 
! call h5file%create_group('global_F')
! call h5file%write_gl(f_global,'f_gl')
! call h5file%close_group('global_F')
! 
! call h5file%create_group('global_G')
! call h5file%write_gl(g_global,1,hsize,'gl')
! call h5file%close_group('global_G')

! arr_1d = 3.1415
! call h5file%create_group('Grid')
! call h5file%write_gl(scale,'scale')
! call h5file%write_gl(arr_1d,'arr_pi')
! call h5file%write_gl(xsize,'xsize')
! call h5file%write_gl(ysize,'ysize')
! call h5file%write_gl(zsize,'zsize')
! call h5file%write_gl(.false.,'logical_f')
! call h5file%write_gl(.true.,'logical_t')
! call h5file%write_gl('write a trial string','string')
! call h5file%close_group('Grid')

!call h5file%close_file(file_name)

!cleanup

call domain%dealloc(g)
call domain%dealloc(f)

call domain%finalize()

!HDF5 I/O finilize
!call h5file%finalize()

call comm%finalize()

 contains

subroutine updateField(scale)
    implicit none

    real(kind=PREC_F90), intent(in) :: scale
    integer i,j,k,h

    do k=domain%get_zs(),domain%get_ze()
       do j=domain%get_ys(),domain%get_ye()
          do i=domain%get_xs(),domain%get_xe()
            f(i,j,k)=f(i,j,k)*scale
!              f(i,j,k)=f(i,j,k)+scale*(f(i,j,k+1)+f(i,j,k-1)-2*f(i,j,k))   !1D Heat Flow
!             f(i,j,k)=f(i,j,k)+scale*(f(i,j,k+1)+f(i,j,k-1)-2*f(i,j,k)+f(i,j+1,k)+f(i,j-1,k)-2*f(i,j,k))   !2D Heat Flow
!              do h=1,hsize
!                g(h,i,j,k)=g(h,i,j,k)*scale
! !                 g(h,i,j,k)=g(h,i,j,k)+scale*(g(h,i,j,k+1)+g(h,i,j,k-1)-2*g(h,i,j,k))   !1D Heat Flow
! !                g(h,i,j,k)=g(h,i,j,k)+scale*(g(h,i,j,k+1)+g(h,i,j,k-1)-2*g(h,i,j,k)+g(h,i,j+1,k)+g(h,i,j-1,k)-2*g(h,i,j,k))   !2D Heat Flow
!              enddo
          enddo
       enddo
    enddo
 end subroutine updateField

subroutine initializeFunction
    implicit none

    integer i,j,k,h

    do k=domain%get_zs(),domain%get_ze()
       do j=domain%get_ys(),domain%get_ye()
          do i=domain%get_xs(),domain%get_xe()
!          f(i,j,k) = domain%mpirank()+1
          f(i,j,k) = 0.5_rk
!           do h=1,hsize
! !             g(h,i,j,k)=domain%mpirank()+1
!              g(h,i,j,k) = 0.5_rk
!           enddo
        enddo
      enddo
    enddo
    
 end subroutine initializeFunction
 
subroutine printFGlobal
    implicit none

    integer i,j,k,h
    
    if(comm%isRoot()) then
    do k=1,zsize
       do j=1,ysize
          do i=1,xsize
           print '(A2,I3,A1,I3,A1,I3,A4,F8.5)',&
                 'f(',i,',',j,',',k,') = ',f_global(i,j,k) 
        enddo
      enddo
    enddo
    endif
    
   !  if(comm%isRoot()) then
   !  do k=1,zsize
   !     do j=1,ysize
   !        do i=1,xsize
   !          do h=1,hsize
   !            print '(A2,I3,A2,I3,A1,I3,A1,I3,A4,F8.5)',&
   !                  'g(',h,',',i,',',j,',',k,') = ',g_global(h,i,j,k) 
   !          enddo    
   !      enddo
   !    enddo
   !  enddo
   !  endif
 end subroutine printFGlobal
 
subroutine printFlocal
    implicit none

    integer i,j,k,h
    
    if(comm%isRoot()) then
    do k=domain%get_zs(),domain%get_ze()
       do j=domain%get_ys(),domain%get_ye()
          do i=domain%get_xs(),domain%get_xe()
           print '(I3,A2,I3,A1,I3,A1,I3,A4,F8.5)',&
                 domain%mpirank(),'f(',i,',',j,',',k,') = ',f(i,j,k) 
        enddo
      enddo
    enddo
    endif
    
 end subroutine printFlocal


end program main

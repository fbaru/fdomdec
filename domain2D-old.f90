
module grid
  implicit none
  integer, parameter :: zmax=12, ymax=25, xmax=1, tmax=10000
  double precision :: dz, dy, dx, dz2, dy2, dx2, dz2i, dy2i, dx2i 
  integer :: zstart = 1				!Starting point of the indexing
  integer :: ystart = 1
  integer :: xstart = 1	
  integer :: zsize, ysize, xsize		!Array sizes
  double precision, dimension(:,:,:), allocatable :: phi, phin
end module grid

module cart_mpi
  implicit none
  integer :: comm_cart
  integer, parameter :: cartDim = 2		!Dimensionality of the cartesian grid
  integer :: b1 = 1				!Number of ghosts cell
  integer :: zdim, ydim, xdim			!Dimensions of the cartesian grid
  integer :: dimsCart(3)			!Dimensions of the cartesian grid
  integer :: coords(3)				!Local coordinates
  logical :: periods(3), reorder
  integer :: left, right, lower, upper
  integer :: vertical_border, horizontal_border 
  integer :: zcoord, ycoord, xcoord		!Coordinates of the cartesian grid
  integer :: zinner, yinner, xinner		!Width of each interval
  integer :: zinner1, yinner1, xinner1	        !Auxiliary variables
  integer :: zouter, youter, xouter		!Outer dimension of each interval
  integer :: zs, ze, ys, ye, xs, xe		!Start and end local index
  integer :: zin, yin, xin			!Number of intervals in each dimension
end module cart_mpi

program main
  
  use grid
  use mpi
  use cart_mpi

  implicit none

  integer :: ierror
  integer :: size, rank
  integer :: x,y,z
  call MPI_INIT(ierror)
  call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierror)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, size, ierror)

  if(cartDim .eq. 2) then
	xsize = (1 + xmax - xstart) 
	ysize = (1 + ymax - ystart) 
	zsize = (1 + zmax - zstart) 
       
        xdim = xmax
	ydim = int(sqrt(real(size)))
	zdim = int(sqrt(real(size)))

! Test if size is   1, 4, 9, 16, 25, ... or 2, 8, 18, 32, 50, ...
	if((ydim*zdim).ne.(size)) then
          ydim = int(sqrt(real(size/2)))
          zdim = 2*ydim
        end if
        if((ydim*zdim).ne.(size)) then
          if(rank.eq.0) then
            print *, 'Number of nodes must be n*n or 2*n*n with n>0'
          end if
          goto 500
        end if

	if( ( (zsize - 2*b1) .lt. zdim)  .or. &
	    ( (ysize - 2*b1) .lt. ydim) ) then
		if(rank==0) then
			print *, 'Do not use more than', (zsize - 2*b1)*(ysize - 2*b1), ' nodes to compute this application!'
		endif
		goto 500
	endif

!Creation of the 2D cartesian grid
	dimsCart(3) = xdim
  	dimsCart(2) = ydim
  	dimsCart(1) = zdim
  
  	periods(3) = .false.
  	periods(2) = .false.
 	periods(1) = .false.
  
  	reorder = .false.

  	call MPI_CART_CREATE(MPI_COMM_WORLD, cartDim, dimsCart, periods, reorder, comm_cart, ierror)
  	call MPI_COMM_RANK(comm_cart, rank, ierror)
  	call MPI_CART_COORDS(comm_cart, rank, cartDim ,coords, ierror)

!local coordinates 	
	xcoord = 1
  	ycoord = coords(2)
	zcoord = coords(1)

! whole z indecees   |------------- zsize = 1+zmax-zstart ------------|
!    start/end index  ^-zstart                                  zmax-^
! 
! 1. interval        |--- zouter1---|   
!                    |--|--------|--|
!                     b1  zinner1 b1 
!    start/end index  ^-zs      ze-^ 
! 2. interval                 |--|--------|--|   
! 3. interval                          |--|--------|--| 
! 4. interval                                   |--|-------|--| 
!                                                   zinner0 
! 5. interval = dimsCart's interval                     |--|-------|--|
!
! In each iteration on each interval, the inner area is computed
! by using the values of the last iteration in the whole outer area. 
! 
! zcoord = number of intervals - 1
!
! To fit exactly into isize, we use zin1 intervals of width zinner1
! and (dim-zin) intervals of width (zinner1 - 1) 
!
! zsize <= 2*b1 + zdim * zinner1  
!
! ==>   zinner1 >= (zsize - 2*b1) / dim
! ==>   smallest valid integer value:
        zinner1 = (zsize - 2*b1 - 1)/ zdim + 1
	zin     =  zsize - 2*b1 - zdim*(zinner1 - 1)
        if(zcoord .lt. zin) then
		zinner = zinner1
		zs = zstart + zcoord*zinner
	else
		zinner = zinner1 - 1
		zs = zstart + zin*zinner1 + (zcoord - zin) *zinner
	endif
	zouter = zinner + 2*b1
        ze = zs + zouter - 1	
!The same for y coordinate
	yinner1 = (ysize - 2*b1 - 1)/ ydim + 1
	yin     =  ysize - 2*b1 - ydim*(yinner1 - 1)
        if(ycoord .lt. yin) then
		yinner = yinner1
		ys = ystart + ycoord*yinner
	else
		yinner = yinner1 - 1
		ys = ystart + yin*yinner1 + (ycoord - yin) *yinner
	endif
	youter = yinner + 2*b1
        ye = ys + youter - 1	

	xs = xstart
        xe = xmax	
		
!       write(*,*) 'PE = ', rank,' zs = ', zs, ' ze = ', ze
!	write(*,*) 'PE = ', rank, ' zin = ', zin, ' of dim = ', zinner
!	write(*,*) 'PE = ', rank, ' zouter = ', zouter

!Allocate local arrays
        allocate(phi (xs:xe,ys:ye,zs:ze))
  	allocate(phin(xs:xe,ys+b1:ye-b1,zs+b1:ze-b1))

	call initialize
	
	call integrate(10)
	
  else
	write(*,*) 'Only 2D Cartesian grid implemented!' 
	goto 500
  endif

  deallocate(phi)
  deallocate(phin)

500 call MPI_FINALIZE(ierror)

end program 	

subroutine initialize
  use grid
  use mpi
  use cart_mpi
  implicit none
  integer :: z,y,x
  integer :: rank, ierror
  
   
  dz = 1.0/zmax
  dy = 1.0/ymax
  dx = 1.0/xmax

  dz2 = dz*dz
  dy2 = dy*dy
  dx2 = dx*dx

  dz2i = 1.0/dz2
  dy2i = 1.0/dy2
  dx2i = 1.0/dx2

!Start value 0.0
  do z = zs, min(ze,zmax-1)
    do y = max(1,ys), min(ye,ymax-1)
         phi(1,y,z) = 0.0
    enddo
  enddo

!Boundary conditions
  if(ze .eq. zmax) then
    do y = ys, ye
        phi(1,y,zmax) = 1.0
    enddo
  endif
!Start value dz
  if(ys .eq. 0) then
     phi(1, 0, zs ) = zs*dz
     do z = zs+1, min(ze,zmax-1)
        phi(1, 0, z) = phi(1, 0, z-1) + dz
    enddo
  endif

  if(ye .eq. ymax) then
     phi(1, ymax, zs) = zs*dz
    do z = zs+1, min(ze,zmax-1)
       phi(1, ymax, z) = phi(1, ymax, z-1) + dz
    enddo
   endif
  
!  call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierror)
!  if(rank==0) then 
!    do z = zs, ze
!     do y = ys, ye
!       write(*,*), y,z, phi(1, y, z)
!     enddo
!    enddo
!   endif

  return
end subroutine initialize

subroutine integrate(stride)

  use grid
  use mpi
  use cart_mpi
  implicit none
  integer :: ierror
  integer :: stride
  integer :: it
  double precision :: dphimax, dphi, dt, dphimaxpartial
  double precision :: eps
  integer :: z,y,x
  integer :: size, rank
  
  call MPI_COMM_SIZE(MPI_COMM_WORLD, size, ierror)
  eps = 1e-8
  dt = min(dz*dz,dy*dy)/4.0
  
  call createCommunication
  
  do it=1,tmax
    dphimax = 0.0
      do z = zs+b1, ze-b1
        do y = ys+b1, ye-b1
	  dphi = (phi(1,y+1,z) + phi(1,y-1,z) - 2.* phi(1,y,z) )*dy2i + &
                 (phi(1,y,z+1) + phi(1,y,z-1) - 2.* phi(1,y,z) )*dz2i
          dphi = dphi * dt
          dphimax = max(dphimax,dphi)
          phin(1,y,z) = phi(1,y,z) + dphi
        enddo
      enddo

      do z=zs+b1,ze-b1
        do y=ys+b1,ye-b1
          phi(1,y,z)=phin(1,y,z)
        enddo
      enddo

    if (mod(it,stride) .eq. 0) then 
       if (size .gt. 1) then 
          dphimaxpartial = dphimax
          call MPI_ALLREDUCE(dphimaxpartial, dphimax, 1, MPI_DOUBLE_PRECISION, MPI_MAX, comm_cart, ierror)
       endif 
       if(dphimax.lt.eps) goto 10
       endif 
    
    call exchangeGhosts

  enddo

10 continue

call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierror)
 if(rank==0) then 
    do z = zs, ze
     do y = ys, ye
        write(*,*),y,z, phi(1, y, z)
       enddo
    enddo
   endif

  return
end subroutine integrate


subroutine createCommunication

  use grid
  use mpi
  use cart_mpi
  implicit none
  
  integer :: ierror

  call MPI_CART_SHIFT(comm_cart, 0, 1, lower, upper, ierror)
  call MPI_CART_SHIFT(comm_cart, 1, 1, left, right, ierror)
  
!Create a MPI Vector of count=zinner, blocklegth=b1, stride=youter for the z vertical border
  call MPI_TYPE_VECTOR(zinner, b1, youter, MPI_DOUBLE_PRECISION, vertical_border,ierror)
  call MPI_TYPE_COMMIT(vertical_border, ierror)

!Create a MPI Vector of count=b1, blocklegth=yinner, stride=youter for the y horizontal border
  call MPI_TYPE_VECTOR(b1, yinner, youter, MPI_DOUBLE_PRECISION, horizontal_border,ierror)
  call MPI_TYPE_COMMIT(horizontal_border, ierror)

  return
end subroutine createCommunication


subroutine exchangeGhosts
  use grid
  use mpi
  use cart_mpi
  implicit none
  integer :: req(4), statuses(MPI_STATUS_SIZE,4), ierror

!send and receive to/from upper/lower
  call MPI_IRECV( phi(1, ys+b1, zs), 1, horizontal_border, lower, MPI_ANY_TAG, comm_cart, req(1), ierror)

  call MPI_IRECV( phi(1, ys+b1, ze), 1, horizontal_border, upper, MPI_ANY_TAG, comm_cart, req(2),ierror)

  call MPI_ISEND( phi(1, ys+b1, ze-b1), 1, horizontal_border, upper, 0, comm_cart, req(3), ierror)
  
  call MPI_ISEND( phi(1, ys+b1, zs+b1), 1, horizontal_border, lower, 0, comm_cart, req(4), ierror)
            
  call MPI_WAITALL(4, req, statuses, ierror)

! send and receive to/from left/right

  call MPI_IRECV( phi(1, ys, zs+b1), 1, vertical_border, left,  MPI_ANY_TAG, comm_cart, req(1), ierror)

  call MPI_IRECV( phi(1, ye, zs+b1), 1, vertical_border, right, MPI_ANY_TAG, comm_cart, req(2), ierror)

  call MPI_ISEND( phi(1, ye-b1, zs+b1), 1, vertical_border, right, 0, comm_cart, req(3), ierror)
  
  call MPI_ISEND( phi(1, ys+b1, zs+b1), 1, vertical_border, left , 0, comm_cart, req(4), ierror)
            
  call MPI_WAITALL(4, req, statuses, ierror)

  return
end subroutine exchangeGhosts








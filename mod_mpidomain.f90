!-----------------------------------------------------------------
!
! Basic data structures and functionality for an MPI-based,
! one and two-dimensional domain decomposition of a three-dimensional,
! Cartesian (x,y,z) grid in to (y,z) domains ("x-pencils")
!
! author: Markus Rampp (RZG)
!         with ideas from Fabio Baruffa (RZG) 
!         and from http://www.2decomp.org/
!
! author: Fabio Baruffa (RZG)
!	o change the exchange halos algorithm not using MPI datatypes 
!       o generalize to 3D and 1D domain decomposition 
!       
!
! Todo: o error handling
!       o documentation
!       o performance verification
!       o stress testing 
!       o code streamline, ...  
!-----------------------------------------------------------------
module mod_mpidomain
  
  use precision
#ifdef MPICODE
  use mpi
#endif
  use my_mpi

  private

!-----------------------------------------------------------------
!
! the central class
!
!-----------------------------------------------------------------
  type, public :: mpidomain

     private

!== private fields ==
     integer, public :: mpi_comm_cart
     integer, private :: nh
     integer, private :: xs,xe,ys,ye,zs,ze   ! start/end coordinates for the local domain including ghost cells
     integer, private :: xlft, xrgt	     ! left/right point of each subdomain (excluding ghost cells)
     integer, private :: ylft, yrgt
     integer, private :: zlft, zrgt
     integer, private :: nx,ny,nz            ! size of sub-domain
     integer, private :: gx,gy,gz            ! global size of the domain
     integer, private :: rank                ! MPI rank
     integer, private :: nproc
     integer, private :: coord(3)            ! MPI rank coordinates (x,y,z)
     logical, private :: periodic(3)         ! periodic boundaries in cart. topology?
     integer, private :: mpi_cart_dim(3)
     integer, private :: cart_dimens	     ! Dimensionality of the domain decomposition
     integer, private :: maxBufLen	     ! Buffer lenght for send/receive
     integer, private :: sendmsg(-1:1,0:2,1:2,0:2), recvmsg(-1:1,0:2,1:2,0:2)
                                             ! (disp,dir,start:end, x, y or z)
     integer, private :: xStartS(4), xEndS(4)
     integer, private :: yStartS(4), yEndS(4)
     integer, private :: zStartS(4), zEndS(4)
     integer, private :: xStartR(4), xEndR(4)
     integer, private :: yStartR(4), yEndR(4)
     integer, private :: zStartR(4), zEndR(4)
     
     integer, private :: source(4), dest(4)

     real(kind=PREC_F90), allocatable :: fieldSend1(:), fieldRecv1(:)
     real(kind=PREC_F90), allocatable :: fieldSend2(:), fieldRecv2(:)
     real(kind=PREC_F90), allocatable :: fieldSend3(:), fieldRecv3(:)
     real(kind=PREC_F90), allocatable :: fieldSend4(:), fieldRecv4(:)

   contains
     private
!== private methods ==
     procedure, private :: create       ! initialize the domain
     procedure, private :: alloc_3d
     procedure, private :: dealloc_3d
     procedure, private :: alloc_4d
     procedure, private :: dealloc_4d
     procedure, private :: updateHalos_3darray
     procedure, private :: updateHalos_4darray
     procedure, private :: sendRecvLimit
     procedure, private :: copySendBuffer_3d 
     procedure, private :: copyRecvBuffer_3d
     procedure, private :: copySendBuffer_4d 
     procedure, private :: copyRecvBuffer_4d
     
     procedure, private :: updateGlobal_3d
     procedure, private :: updateGlobal_4d

     procedure, private :: copy2SendBuf_all
     procedure, private :: copy2RecvBuf_all

!== public methods ==
     procedure, public :: finalize     ! clean up
     procedure, public :: info         ! print information about the domain
     procedure, public :: infoDomain   ! print information about the size of the domain decomposition
     procedure, public :: isHalocell   ! convenience function 

! communication routine
     !generic, public :: updateHalos => updateHalos_3darray,updateHalos_4darray
     generic, public :: exchangeHalos => updateHalos_3darray
     !,updateHalos_4darray
! convenience method for array allocation     
     generic, public :: alloc   => alloc_3d,alloc_4d
     generic, public :: dealloc => dealloc_3d, dealloc_4d
     
     generic, public :: copySendBuffer => copySendBuffer_3d, copySendBuffer_4d
     generic, public :: copyRecvBuffer => copyRecvBuffer_3d, copyRecvBuffer_4d

     generic, public :: copy2SendBuf => copy2SendBuf_all
     generic, public :: copy2RecvBuf => copy2RecvBuf_all

     
! utility method for gathering domain data into a global array      
     generic, public :: updateGlobal => updateGlobal_3d,updateGlobal_4d     
     
!= getter methods (making fields read-only) =
     procedure, public :: nhalo          !ngc

! start/end local internal coordinates excluding halos and borders
     procedure, public :: xmin                
     procedure, public :: xmax                
     procedure, public :: ymin                
     procedure, public :: ymax                
     procedure, public :: zmin                
     procedure, public :: zmax                
! start/end local coordinates including halos
     procedure, public :: get_xs
     procedure, public :: get_xe
     procedure, public :: get_ys
     procedure, public :: get_ye
     procedure, public :: get_zs
     procedure, public :: get_ze
! size of the subdomain
     procedure, public :: get_lx
     procedure, public :: get_ly
     procedure, public :: get_lz
! size of the global domain
     procedure, public :: get_gx
     procedure, public :: get_gy
     procedure, public :: get_gz
! left/right local coordinates
     procedure, public :: get_xlft
     procedure, public :: get_xrgt
     procedure, public :: get_ylft
     procedure, public :: get_yrgt
     procedure, public :: get_zlft
     procedure, public :: get_zrgt
! MPI related
     procedure, public :: mpirank
     procedure, public :: mpiproc
     procedure, public :: mpicoord
     procedure, public :: mpiperiod

     procedure, public :: get_buflen

  end type mpidomain


   type(mpidomain), public :: domain


  interface mpidomain

     module procedure constructor    

  end interface mpidomain
!-----------------------------------------------------------------


 contains

  function constructor(drank,xsize,ysize,zsize,gs,ierr)
    type(mpidomain) :: constructor

    integer :: drank		      ! dimensionality of the domain decomposition
    integer :: xsize,ysize,zsize,gs   ! global grid size
    integer :: ierr

    call constructor%create(drank,xsize,ysize,zsize,gs,ierr)

  end function constructor


  subroutine create(this,drank,xsize,ysize,zsize,gs,ierr)

!--------------------------------------------------------------------------
!  creates a one,two or three dimensional cartesian communicator topology and 
!  associates it with the newly created domain object (one domain per MPI rank) 
!  The 1D domain decomposition is along z (the slowest index in Fortran, assuming array[t,x,y,z], XY-Plains)
!  The 2D domain decomposition is along z and y (X-Pencils)
!  The 3D domain decomposition is along z, y and x
!--------------------------------------------------------------------------

    implicit none

    class(mpidomain) :: this 

    integer, intent(in) :: drank 		  ! dimensionality of the domain decomposition
    integer, intent(in) :: xsize,ysize,zsize	  ! global grid size
    integer, intent(in) :: gs     		  ! number of ghost cells
    integer, intent(out) :: ierr


    integer :: i, j
    integer :: xs,xe,ys,ye,zs,ze
    integer :: xdim,ydim,zdim
    integer :: xlft, xrgt	                  ! left/right point of each subdomain (excluding ghost cells)
    integer :: ylft, yrgt
    integer :: zlft, zrgt
    integer :: mpi_comm_cart,numproc,myrank


    integer :: dimsCart(3),coords(3)
    logical :: periods(3), reorder
    integer :: xinner,xinner1,xin
    integer :: yinner,yinner1,yin
    integer :: zinner,zinner1,zin

    integer, dimension(1:3) :: msgsize
    integer :: locLenMax, maxBufLen
  
    ierr = 0
    numproc=comm%numproc
    myrank =comm%myrank
    mpi_comm_cart=0
    
    !Creation of the 3D processor grid (square-like) 
    dimsCart = [0,0,0]
    coords   = [0,0,0]
    !These periods define the periodicity of the domain decomposition in each direction
    periods(1) = .false.
    periods(2) = .false.
    periods(3) = .false.
    !It reorders the ranks to improve performance (see MPI documentation)
    reorder = .false.

    if(drank .lt. 3 .and. drank .gt. 0) then
    !1D Domain Decomposition along z direction
#ifdef MPICODE
      call MPI_DIMS_CREATE(numproc,drank,dimsCart,ierr)
    
      call MPI_CART_CREATE(comm%mpicomm,drank,dimsCart,periods,reorder,mpi_comm_cart, ierr)
      call MPI_COMM_RANK(mpi_comm_cart,myrank,ierr)
      call MPI_CART_COORDS(mpi_comm_cart,myrank,drank,coords,ierr)
#endif      
      zdim=dimsCart(1)
      ydim=dimsCart(2)
      xdim=dimsCart(3)
      
 !Divide the subdomains, specifying the indeces xyx-start end xyx-end
      if(zdim /= 0) then
        zinner1 = (zsize -2*gs - 1)/ zdim + 1
        zin     =  zsize -2*gs - zdim*(zinner1 - 1)
        if(coords(1) .lt. zin) then
           zinner = zinner1
           zs = 1 + coords(1)*zinner
        else
           zinner = zinner1 - 1
           zs = 1 + zin*zinner1 + (coords(1) - zin) *zinner
        endif
        ze = zs + zinner + 2*gs - 1
      else
        zs = 1
        ze = zsize
      endif

      if(ydim /= 0) then
          yinner1 = (ysize -2*gs - 1)/ ydim + 1
          yin     =  ysize -2*gs - ydim*(yinner1 - 1)
        if(coords(2) .lt. yin) then
           yinner = yinner1
           ys = 1 + coords(2)*yinner
        else
           yinner = yinner1 - 1
           ys = 1 + yin*yinner1 + (coords(2) - yin) *yinner
        endif
        ye = ys + yinner + 2*gs - 1
      else
        ys = 1
        ye = ysize
      endif
      
      if(xdim /= 0) then
          xinner1 = (xsize -2*gs - 1)/ xdim + 1
          xin     =  xsize -2*gs - xdim*(xinner1 - 1)
        if(coords(3) .lt. xin) then
           xinner = xinner1
           xs = 1 + coords(3)*xinner
        else
           xinner = xinner1 - 1
           xs = 1 + xin*xinner1 + (coords(3) - xin) *xinner
        endif
        xe = xs + xinner + 2*gs - 1
      else
        xs = 1
        xe = xsize
      endif

      this%coord(1)=coords(1)  !z 
      this%coord(2)=coords(2)  !y
      this%coord(3)=coords(3)  !x
      
      this%mpi_cart_dim(1)=zdim
      this%mpi_cart_dim(2)=ydim
      this%mpi_cart_dim(3)=xdim
      
      this%nx = xe-xs+1
      this%ny = ye-ys+1
      this%nz = ze-zs+1
      
    ! Compute maximum buffer length for send/receive
      maxBufLen = 0
      locLenMax = 0
      msgsize(1) = this%ny * this%nz * gs
      LocLenMax = max(LocLenMax,msgsize(1))
      msgsize(2) = this%nx * this%nz * gs
      LocLenMax = max(LocLenMax,msgsize(2))
      msgsize(3) = this%nx * this%ny * gs
      LocLenMax = max(LocLenMax,msgsize(3))    
#ifdef MPICODE    
      call MPI_REDUCE(LocLenMax, maxBufLen, 1, MPI_INTEGER, MPI_MAX, 0, mpi_comm_cart, ierr)
      call MPI_BCAST(maxBufLen, 1, MPI_INTEGER, 0, mpi_comm_cart, ierr)
#endif    
      this%maxBufLen = maxBufLen
     
  else 
      write(*,*) '3D Domain Decomposition non yet implemented!!!'
  endif
  
    if (xs .eq. 1) then
     xlft=xs
    else
     xlft=xs+gs
    endif
    if (xe .eq. xsize) then
     xrgt=xe
    else
     xrgt=xe-gs
    endif
    
    if (ys .eq. 1) then
     ylft=ys
    else
     ylft=ys+gs
    endif
    if (ye .eq. ysize) then
     yrgt=ye
    else
     yrgt=ye-gs
    endif
    
    if (zs .eq. 1) then
     zlft=zs
    else
     zlft=zs+gs
    endif
    if (ze .eq. zsize) then
     zrgt=ze
    else
     zrgt=ze-gs
    endif	
    
  !populate class members 
    this%cart_dimens=drank
    this%rank=myrank
    this%nproc=numproc
    this%periodic=periods
    this%mpi_comm_cart=mpi_comm_cart
    
    this%xs = xs      
    this%xe = xe
    this%ys = ys
    this%ye = ye
    this%zs = zs
    this%ze = ze
    
    this%xlft = xlft      
    this%xrgt = xrgt
    this%ylft = ylft
    this%yrgt = yrgt
    this%zlft = zlft
    this%zrgt = zrgt
    
    this%nh = gs
    
    this%gx = xsize
    this%gy = ysize
    this%gz = zsize

    
    call MPI_CART_SHIFT(this%mpi_comm_cart, 0 , -1, this%source(1), this%dest(1), ierr)
    call MPI_CART_SHIFT(this%mpi_comm_cart, 1 , -1, this%source(2), this%dest(2), ierr)
    call MPI_CART_SHIFT(this%mpi_comm_cart, 0 , 1,  this%source(3), this%dest(3), ierr)
    call MPI_CART_SHIFT(this%mpi_comm_cart, 1 , 1,  this%source(4), this%dest(4), ierr)

     ! write(*,*), 'Rank = ', comm%myrank, 'source(1) = ', source(1), 'dest(1) = ', dest(1)
     ! write(*,*), 'Rank = ', comm%myrank, 'source(2) = ', source(2), 'dest(2) = ', dest(2)
     ! write(*,*), 'Rank = ', comm%myrank, 'source(3) = ', source(3), 'dest(3) = ', dest(3)
     ! write(*,*), 'Rank = ', comm%myrank, 'source(4) = ', source(4), 'dest(4) = ', dest(4)

!Call the function which computes the extrema for the halo exchange    
    call this%sendRecvLimit
    

    allocate(this%fieldRecv1(1:this%maxBufLen))
    allocate(this%fieldSend1(1:this%maxBufLen))
    allocate(this%fieldRecv2(1:this%maxBufLen))
    allocate(this%fieldSend2(1:this%maxBufLen))
    allocate(this%fieldRecv3(1:this%maxBufLen))
    allocate(this%fieldSend3(1:this%maxBufLen))
    allocate(this%fieldRecv4(1:this%maxBufLen))
    allocate(this%fieldSend4(1:this%maxBufLen))

  end subroutine create
!-----------------------------------------------------------------

  subroutine finalize(this)

    class(mpidomain), intent(inout) :: this 
    integer ierr
       
    deallocate(this%fieldSend1)
    deallocate(this%fieldRecv1)
    deallocate(this%fieldSend2)
    deallocate(this%fieldRecv2)
    deallocate(this%fieldSend3)
    deallocate(this%fieldRecv3)
    deallocate(this%fieldSend4)
    deallocate(this%fieldRecv4)

  end subroutine finalize

!-----------------------------------------------------------------
  subroutine info(this)
    implicit none
    class(mpidomain), intent(in) :: this 
    integer :: i
    
    do i=0,comm%numproc-1
       if(i.eq.0 .and. comm%myrank.eq.0) then 
          print '(11X, A13)','(  z  y  x )'
       endif
       if(comm%myrank.eq.i) then 
          print '(A7,I5,A1,3I3,A2,10(A5,I4,1X),A9,1F6.3,A,6I3)',&
               &'Rank:',comm%myrank,'(',this%coord,')',&
               &'xs:',this%xs, 'xe:',this%xe,&
               &'ys:',this%ys, 'ye:',this%ye,&
               &'zs:',this%zs, 'ze:',this%ze,&
               &'nx:',this%nx, 'ny:',this%ny,'nz:',this%nz,'nh',this%nh
          print '(A7,I5,A1,3I3,A2,10(A5,I4,1X),A9,1F6.3,A,6I3)',&
               &'Rank:',comm%myrank,'(',this%coord,')',&
               &'xlft:',this%xlft, 'xrgt:',this%xrgt,&
               &'ylft:',this%ylft, 'yrgt:',this%yrgt,&
               &'zlft:',this%zlft, 'zrgt:',this%zrgt,&
               &'nx:',this%nx, 'ny:',this%ny,'nz:',this%nz,'nh',this%nh
       endif
#ifdef MPICODE
       call MPI_BARRIER(comm%mpicomm,ierr)
#endif
    enddo

  end subroutine info
!-----------------------------------------------------------------
  subroutine infoDomain(this)

    class(mpidomain), intent(in) :: this 
    if(this%rank == 0) then
        print '(42X, A13)','(  z  y  x )' 
        print '(A35,I5,A4,3I3,A2)',&
         &'Domain Decomp Dimension: ',this%cart_dimens,'(',this%mpi_cart_dim,' )'   
   endif
#ifdef MPICODE
  call MPI_Barrier(comm%mpicomm, ierr)
#endif 
  end subroutine infoDomain
!-----------------------------------------------------------------
  subroutine sendRecvLimit(this)
!Define for every subdomain the indeces (lower and upper) of the halo region to
!send and the one to receive, for every direction (xyz) and side (-+). 
    implicit none
    class(mpidomain) :: this 
    integer :: b1,xs,xe,ys,ye,zs,ze
    integer :: Point
    
    this%sendmsg(:,:,:,:) = 0
    this%recvmsg(:,:,:,:) = 0
    
    xs = this%xs
    xe = this%xe
   
    ys = this%ys
    ye = this%ye
 
    zs = this%zs
    ze = this%ze
    
    b1 = this%nh
    Point = b1 - 1
    
    if (this%cart_dimens == 1) then
     !(disp,dir,start:end,x, y or z) 
     ! disp: is the displacement in the direction positive or negative
     ! dir : corresponds to the direction z. In qD domain decomposition
     !       dir-1 = 0 for z 
 this%sendmsg(-1,0,1,0) = xs
 this%sendmsg(-1,0,2,0) = xe     
 this%sendmsg(-1,0,1,1) = ys
 this%sendmsg(-1,0,2,1) = ye 
 this%sendmsg(-1,0,1,2) = zs+b1 
 this%sendmsg(-1,0,2,2) = zs+b1 + Point
 
 this%recvmsg(-1,0,1,0) = xs
 this%recvmsg(-1,0,2,0) = xe
 this%recvmsg(-1,0,1,1) = ys
 this%recvmsg(-1,0,2,1) = ye
 this%recvmsg(-1,0,1,2) = ze - Point
 this%recvmsg(-1,0,2,2) = ze
 
 this%sendmsg( 1,0,1,0) = xs 
 this%sendmsg( 1,0,2,0) = xe 
 this%sendmsg( 1,0,1,1) = ys 
 this%sendmsg( 1,0,2,1) = ye 
 this%sendmsg( 1,0,1,2) = ze-b1 - Point
 this%sendmsg( 1,0,2,2) = ze-b1
 
 this%recvmsg( 1,0,1,0) = xs
 this%recvmsg( 1,0,2,0) = xe
 this%recvmsg( 1,0,1,1) = ys
 this%recvmsg( 1,0,2,1) = ye
 this%recvmsg( 1,0,1,2) = zs
 this%recvmsg( 1,0,2,2) = zs + Point
        
    else if(this%cart_dimens == 2) then
     !(disp,dir,start:end,x, y or z) 
     ! disp: is the displacement in the direction positive or negative
     ! dir : corresponds to the direction. In 2D domain decomposition
     !       dir-1 = 0 for y
     !       dir-1 = 1 for z
 this%sendmsg(-1,0,1,0) = xs
 this%sendmsg(-1,0,2,0) = xe
 this%sendmsg(-1,0,1,1) = ys+b1
 this%sendmsg(-1,0,2,1) = ys+b1 + Point
 this%sendmsg(-1,0,1,2) = zs+b1
 this%sendmsg(-1,0,2,2) = ze-b1
 
 this%recvmsg(-1,0,1,0) = xs
 this%recvmsg(-1,0,2,0) = xe
 this%recvmsg(-1,0,1,1) = ye - Point
 this%recvmsg(-1,0,2,1) = ye
 this%recvmsg(-1,0,1,2) = zs+b1
 this%recvmsg(-1,0,2,2) = ze-b1
 
 this%sendmsg(-1,1,1,0) = xs
 this%sendmsg(-1,1,2,0) = xe 
 this%sendmsg(-1,1,1,1) = ys+b1
 this%sendmsg(-1,1,2,1) = ye-b1 
 this%sendmsg(-1,1,1,2) = zs+b1
 this%sendmsg(-1,1,2,2) = zs+b1 + Point
 
 this%recvmsg(-1,1,1,0) = xs
 this%recvmsg(-1,1,2,0) = xe
 this%recvmsg(-1,1,1,1) = ys+b1
 this%recvmsg(-1,1,2,1) = ye-b1
 this%recvmsg(-1,1,1,2) = ze - Point
 this%recvmsg(-1,1,2,2) = ze

 this%sendmsg( 1,0,1,0) = xs
 this%sendmsg( 1,0,2,0) = xe 
 this%sendmsg( 1,0,1,1) = ye-b1 - Point
 this%sendmsg( 1,0,2,1) = ye-b1
 this%sendmsg( 1,0,1,2) = zs+b1
 this%sendmsg( 1,0,2,2) = ze-b1

 this%recvmsg( 1,0,1,0) = xs
 this%recvmsg( 1,0,2,0) = xe
 this%recvmsg( 1,0,1,1) = ys
 this%recvmsg( 1,0,2,1) = ys + Point
 this%recvmsg( 1,0,1,2) = zs+b1
 this%recvmsg( 1,0,2,2) = ze-b1
 
 this%sendmsg( 1,1,1,0) = xs
 this%sendmsg( 1,1,2,0) = xe
 this%sendmsg( 1,1,1,1) = ys+b1
 this%sendmsg( 1,1,2,1) = ye-b1
 this%sendmsg( 1,1,1,2) = ze-b1 - Point
 this%sendmsg( 1,1,2,2) = ze-b1
 
 this%recvmsg( 1,1,1,0) = xs
 this%recvmsg( 1,1,2,0) = xe 
 this%recvmsg( 1,1,1,1) = ys+b1
 this%recvmsg( 1,1,2,1) = ye-b1
 this%recvmsg( 1,1,1,2) = zs
 this%recvmsg( 1,1,2,2) = zs + Point
      endif
 !(disp,dir,start:end,x,y or z) 
      this%xStartS(1) = this%sendmsg(-1,0,1,0)
      this%xEndS(1)   = this%sendmsg(-1,0,2,0)
      this%yStartS(1) = this%sendmsg(-1,0,1,1)
      this%yEndS(1)   = this%sendmsg(-1,0,2,1)
      this%zStartS(1) = this%sendmsg(-1,0,1,2)
      this%zEndS(1)   = this%sendmsg(-1,0,2,2)	
!(disp,dir,start:end,x,y or z)
      this%xStartR(1) = this%recvmsg(-1,0,1,0)
      this%xEndR(1)   = this%recvmsg(-1,0,2,0)
      this%yStartR(1) = this%recvmsg(-1,0,1,1)
      this%yEndR(1)   = this%recvmsg(-1,0,2,1)
      this%zStartR(1) = this%recvmsg(-1,0,1,2)
      this%zEndR(1)   = this%recvmsg(-1,0,2,2)
 !(disp,dir,start:end,x,y or z) 
      this%xStartS(2) = this%sendmsg(-1,1,1,0)
      this%xEndS(2)   = this%sendmsg(-1,1,2,0)
      this%yStartS(2) = this%sendmsg(-1,1,1,1)
      this%yEndS(2)   = this%sendmsg(-1,1,2,1)
      this%zStartS(2) = this%sendmsg(-1,1,1,2)
      this%zEndS(2)   = this%sendmsg(-1,1,2,2)	
 !(disp,dir,start:end,x,y or z)
      this%xStartR(2) = this%recvmsg(-1,1,1,0)
      this%xEndR(2)   = this%recvmsg(-1,1,2,0)
      this%yStartR(2) = this%recvmsg(-1,1,1,1)
      this%yEndR(2)   = this%recvmsg(-1,1,2,1)
      this%zStartR(2) = this%recvmsg(-1,1,1,2)
      this%zEndR(2)   = this%recvmsg(-1,1,2,2)
 !(disp,dir,start:end,x,y or z) 
      this%xStartS(3) = this%sendmsg(1,0,1,0)
      this%xEndS(3)   = this%sendmsg(1,01,2,0)
      this%yStartS(3) = this%sendmsg(1,0,1,1)
      this%yEndS(3)   = this%sendmsg(1,0,2,1)
      this%zStartS(3) = this%sendmsg(1,0,1,2)
      this%zEndS(3)   = this%sendmsg(1,0,2,2)	
        !(disp,dir,start:end,x,y or z)
      this%xStartR(3) = this%recvmsg(1,0,1,0)
      this%xEndR(3)   = this%recvmsg(1,0,2,0)
      this%yStartR(3) = this%recvmsg(1,0,1,1)
      this%yEndR(3)   = this%recvmsg(1,0,2,1)
      this%zStartR(3) = this%recvmsg(1,0,1,2)
      this%zEndR(3)   = this%recvmsg(1,0,2,2)
        !(disp,dir,start:end,x,y or z) 
      this%xStartS(4) = this%sendmsg(1,1,1,0)
      this%xEndS(4)   = this%sendmsg(1,1,2,0)
      this%yStartS(4) = this%sendmsg(1,1,1,1)
      this%yEndS(4)   = this%sendmsg(1,1,2,1)
      this%zStartS(4) = this%sendmsg(1,1,1,2)
      this%zEndS(4)   = this%sendmsg(1,1,2,2)	
        !(disp,dir,start:end,x,y or z)
      this%xStartR(4) = this%recvmsg(1,1,1,0)
      this%xEndR(4)   = this%recvmsg(1,1,2,0)
      this%yStartR(4) = this%recvmsg(1,1,1,1)
      this%yEndR(4)   = this%recvmsg(1,1,2,1)
      this%zStartR(4) = this%recvmsg(1,1,1,2)
      this%zEndR(4)   = this%recvmsg(1,1,2,2)

  end subroutine sendRecvLimit
!-----------------------------------------------------------------

  subroutine alloc_3d(this,arr,withhalos)

    class(mpidomain) :: this 
    real(kind=PREC_F90), allocatable :: arr(:,:,:)
    logical, intent(in) :: withhalos
    
    integer gs

    if(withhalos) then
      allocate (arr(this%xs:this%xe,this%ys:this%ye,this%zs:this%ze))
    else
      allocate (arr(this%xlft:this%xrgt,this%ylft:this%yrgt,this%zlft:this%zrgt))
    endif
#ifdef DEBUG_MPI
  call MPI_Barrier(comm%mpicomm, ierr)
    print '(I3,A,(I3,A,I3),A,(I3,A,I3),A,(I3,A,I3))',this%rank,' allocated array: ',&
         &lbound(arr,1),':',ubound(arr,1),',',&
         &lbound(arr,2),':',ubound(arr,2),',',&
         &lbound(arr,3),':',ubound(arr,3)
  call MPI_Barrier(comm%mpicomm, ierr)
#endif
 
    
  end subroutine alloc_3d
  
  subroutine dealloc_3d(this,arr)

    class(mpidomain) :: this 
    real(kind=PREC_F90), allocatable :: arr(:,:,:)

#ifdef DEBUG_MPI
    call MPI_Barrier(comm%mpicomm, ierr)
    print '(I3,A,(I3,A,I3),A,(I3,A,I3),A,(I3,A,I3))',this%rank,' deallocated array: ',&
         &lbound(arr,1),':',ubound(arr,1),',',&
         &lbound(arr,2),':',ubound(arr,2),',',&
         &lbound(arr,3),':',ubound(arr,3)
    call MPI_Barrier(comm%mpicomm, ierr)
#endif
    deallocate (arr)

  end subroutine dealloc_3d
!-----------------------------------------------------------------
!   subroutine updateHalos_3darray(this,f)

!     implicit none
    
!     class(mpidomain) :: this 
!     real(kind=PREC_F90), intent(inout) :: f(this%xs:this%xe, &
!                                             this%ys:this%ye, &
!                                             this%zs:this%ze)
! #ifdef MPICODE
!     real(kind=PREC_F90), allocatable :: fieldSend(:), fieldRecv(:)
!     integer :: req
!     integer :: status(MPI_STATUS_SIZE)
!     integer :: disp, dir
!     integer :: source, dest, tag
!     integer :: xStart, xEnd
!     integer :: yStart, yEnd
!     integer :: zStart, zEnd
!     integer :: ix,iy,iz,I
  
!     allocate(fieldRecv(1:this%maxBufLen))
!     allocate(fieldSend(1:this%maxBufLen))
     
!     do disp = -1, 1, 2
!      do dir = 1,domain_rank(this)
     
!      call MPI_CART_SHIFT(this%mpi_comm_cart, (dir-1) , disp, source, dest, ierr)

! #ifdef DEBUG_MPI  
!      print '(A,I3,2X,A,I3,2X,A,I3,2X,A,I3,2X,A,I3,4X,8I3)',&
!         'Rank: ',this%rank, 'dir-1: ',dir-1 , 'disp: ',disp, 'R From: ',source, 'S to: ', dest, &
! 	         this%sendmsg(disp,dir-1,1,1), this%sendmsg(disp,dir-1,2,1), &
! 	         this%sendmsg(disp,dir-1,1,2), this%sendmsg(disp,dir-1,2,2), &
! 	         this%recvmsg(disp,dir-1,1,1), this%recvmsg(disp,dir-1,2,1), &
! 	         this%recvmsg(disp,dir-1,1,2), this%recvmsg(disp,dir-1,2,2) 
! #endif	         
!      tag = (disp + 1) + dir   	
!      if(source /= MPI_PROC_NULL) then
!        call MPI_IRECV( fieldRecv(1), this%maxBufLen, PREC_MPI, &
!                        source, tag, this%mpi_comm_cart, req, ierr )
!      endif

!      if(dest /= MPI_PROC_NULL) then
!         !(disp,dir,start:end,x,y or z) 
!         xStart = this%sendmsg(disp,dir-1,1,0)
!         xEnd   = this%sendmsg(disp,dir-1,2,0)
!         yStart = this%sendmsg(disp,dir-1,1,1)
!         yEnd   = this%sendmsg(disp,dir-1,2,1)
!         zStart = this%sendmsg(disp,dir-1,1,2)
!         zEnd   = this%sendmsg(disp,dir-1,2,2)	

!        call this%copySendBuffer(fieldSend, this%maxBufLen, xStart, xEnd, yStart ,yEnd, zStart, zEnd, f)
       	
!        call MPI_SEND( fieldSend(1), this%maxBufLen, PREC_MPI, &
!                       dest, tag, this%mpi_comm_cart, ierr)
!      endif 

!      if(source /= MPI_PROC_NULL) then
!        call MPI_WAIT(req, status, ierr)
!         !(disp,dir,start:end,x,y or z)
!         xStart = this%recvmsg(disp,dir-1,1,0)
!         xEnd   = this%recvmsg(disp,dir-1,2,0)
!         yStart = this%recvmsg(disp,dir-1,1,1)
!         yEnd   = this%recvmsg(disp,dir-1,2,1)
!         zStart = this%recvmsg(disp,dir-1,1,2)
!         zEnd   = this%recvmsg(disp,dir-1,2,2)

!        call this%copyRecvBuffer(fieldRecv, this%maxBufLen, xStart, xEnd, yStart, yEnd, zStart, zEnd, f)

!      endif

!      enddo
!    enddo  
      
!   deallocate(fieldSend)
!   deallocate(fieldRecv)
! #endif
!   return
!  end subroutine updateHalos_3darray

subroutine updateHalos_3darray(this,f, reqs, statuses)

    implicit none
    
    class(mpidomain) :: this 
    real(kind=PREC_F90), intent(inout) :: f(this%xs:this%xe, &
                                            this%ys:this%ye, &
                                            this%zs:this%ze)
#ifdef MPICODE

    integer :: req, reqs(8)
    integer :: status(MPI_STATUS_SIZE), statuses(MPI_STATUS_SIZE,8)
    
    integer :: ix,iy,iz,I
  

    !call this%copy2SendBuf_all(f)

    call MPI_IRECV( this%fieldRecv1, this%maxBufLen, PREC_MPI, this%source(1), MPI_ANY_TAG, this%mpi_comm_cart, reqs(1), ierr )
    call MPI_IRECV( this%fieldRecv2, this%maxBufLen, PREC_MPI, this%source(2), MPI_ANY_TAG, this%mpi_comm_cart, reqs(3), ierr )
    call MPI_IRECV( this%fieldRecv3, this%maxBufLen, PREC_MPI, this%source(3), MPI_ANY_TAG, this%mpi_comm_cart, reqs(5), ierr )
    call MPI_IRECV( this%fieldRecv4, this%maxBufLen, PREC_MPI, this%source(4), MPI_ANY_TAG, this%mpi_comm_cart, reqs(7), ierr )
    

    call MPI_ISEND( this%fieldSend4, this%maxBufLen, PREC_MPI, this%dest(4), 0, this%mpi_comm_cart, reqs(8), ierr)
    call MPI_ISEND( this%fieldSend3, this%maxBufLen, PREC_MPI, this%dest(3), 0, this%mpi_comm_cart, reqs(6), ierr)
    call MPI_ISEND( this%fieldSend2, this%maxBufLen, PREC_MPI, this%dest(2), 0, this%mpi_comm_cart, reqs(4), ierr)    
    call MPI_ISEND( this%fieldSend1, this%maxBufLen, PREC_MPI, this%dest(1), 0, this%mpi_comm_cart, reqs(2), ierr)     

    !call MPI_WAITALL(8, reqs, statuses, ierr)
  
    !call this%copy2RecvBuf_all(f)
    

#endif
  return
 end subroutine updateHalos_3darray
 
subroutine copy2SendBuf_all(this,f)
  implicit none
    
    class(mpidomain) :: this 
    real(kind=PREC_F90), intent(in) :: f(this%xs:this%xe, &
                                            this%ys:this%ye, &
                                            this%zs:this%ze)

    if(this%dest(4) /= MPI_PROC_NULL) then
      call this%copySendBuffer(this%fieldSend4, this%maxBufLen, this%xStartS(4), this%xEndS(4), this%yStartS(4) ,this%yEndS(4), this%zStartS(4), this%zEndS(4), f)
    endif 

    if(this%dest(3) /= MPI_PROC_NULL) then
      call this%copySendBuffer(this%fieldSend3, this%maxBufLen, this%xStartS(3), this%xEndS(3), this%yStartS(3) ,this%yEndS(3), this%zStartS(3), this%zEndS(3), f)
    endif 

    if(this%dest(2) /= MPI_PROC_NULL) then
      call this%copySendBuffer(this%fieldSend2, this%maxBufLen, this%xStartS(2), this%xEndS(2), this%yStartS(2), this%yEndS(2), this%zStartS(2), this%zEndS(2), f)
    endif 

    if(this%dest(1) /= MPI_PROC_NULL) then
      call this%copySendBuffer(this%fieldSend1, this%maxBufLen, this%xStartS(1), this%xEndS(1), this%yStartS(1), this%yEndS(1), this%zStartS(1), this%zEndS(1), f)
    endif

end subroutine copy2SendBuf_all

subroutine copy2RecvBuf_all(this,f)
  implicit none
    
    class(mpidomain) :: this 
    real(kind=PREC_F90), intent(out) :: f(this%xs:this%xe, &
                                            this%ys:this%ye, &
                                            this%zs:this%ze)
   if(this%source(1) /= MPI_PROC_NULL) then
      call this%copyRecvBuffer(this%fieldRecv1, this%maxBufLen, this%xStartR(1), this%xEndR(1), this%yStartR(1), this%yEndR(1), this%zStartR(1), this%zEndR(1), f)
    endif
    if(this%source(2) /= MPI_PROC_NULL) then
      call this%copyRecvBuffer(this%fieldRecv2, this%maxBufLen, this%xStartR(2), this%xEndR(2), this%yStartR(2), this%yEndR(2), this%zStartR(2), this%zEndR(2), f)
    endif
    if(this%source(3) /= MPI_PROC_NULL) then
      call this%copyRecvBuffer(this%fieldRecv3, this%maxBufLen, this%xStartR(3), this%xEndR(3), this%yStartR(3), this%yEndR(3), this%zStartR(3), this%zEndR(3), f)
    endif
    if(this%source(4) /= MPI_PROC_NULL) then
      call this%copyRecvBuffer(this%fieldRecv4, this%maxBufLen, this%xStartR(4), this%xEndR(4), this%yStartR(4), this%yEndR(4), this%zStartR(4), this%zEndR(4), f)
    endif

end subroutine copy2RecvBuf_all

 !-----------------------------------------------------------------
  subroutine updateGlobal_3d(this,f,f_global)
! USED ONLY for debugging
! called by all ranks to fill the global array (f_global) with
! values from a local array(f) on the local domain
!
! input:  f local  array (domain, without halos!)
! output: f_global global array (entire grid, with boundaries)
!
! note that this is an auxiliary routine for development only, not
! optimized for performance


    implicit none
    
    class(mpidomain) :: this 
    real(kind=PREC_F90), intent(in)  :: f(this%xs:this%xe, &
                                          this%ys:this%ye, &
                                          this%zs:this%ze)    !local

    real(kind=PREC_F90), intent(inout) :: f_global(:,:,:) !global
#ifdef MPICODE
    real(kind=PREC_F90), allocatable :: f_global_0(:,:,:) !global to process 0
    
    integer :: lsize, gsize
    real(kind=PREC_F90),allocatable, dimension(:) :: mSend,mRecv
    integer :: ix,iy,iz,I,jj,k
    integer :: xStart, xEnd
    integer :: yStart, yEnd
    integer :: zStart, zEnd
    integer :: req(7)

    lsize = (this%zrgt-this%zlft+1)*(this%yrgt-this%ylft+1)*(this%xrgt-this%xlft+1)
    gsize = size(f_global,dim=1)*size(f_global,dim=2)*size(f_global,dim=3)

    allocate(mSend(lsize))
    allocate(mRecv(lsize))
    
    allocate(f_global_0( 1:size(f_global,dim=1),1:size(f_global,dim=2),1:size(f_global,dim=3)) )
    
    
    I=0
    do iz=this%zlft,this%zrgt
      do iy=this%ylft,this%yrgt
	do ix=this%xlft,this%xrgt
          I = I+1
	  mSend(I) = f(ix,iy,iz)
	enddo
      enddo
    enddo


   if(this%mpirank() .eq. 0) then
    I=0 
    do iz=this%zlft,this%zrgt
      do iy=this%ylft,this%yrgt
	     do ix=this%xlft,this%xrgt
          I = I+1
          f_global_0(ix,iy,iz) = mSend(I)
        enddo
      enddo
    enddo
  
    do k=1,(this%mpiproc() -1)
        
      	call MPI_RECV( xStart, 1, MPI_INTEGER, k, 1, this%mpi_comm_cart, req(1), ierr )
	call MPI_RECV( xEnd,   1, MPI_INTEGER, k, 2, this%mpi_comm_cart, req(1), ierr )
	call MPI_RECV( yStart, 1, MPI_INTEGER, k, 3, this%mpi_comm_cart, req(1), ierr )
	call MPI_RECV( yEnd,   1, MPI_INTEGER, k, 4, this%mpi_comm_cart, req(1), ierr )
	call MPI_RECV( zStart, 1, MPI_INTEGER, k, 5, this%mpi_comm_cart, req(1), ierr )
	call MPI_RECV( zEnd,   1, MPI_INTEGER, k, 6, this%mpi_comm_cart, req(1), ierr )
        call MPI_RECV( mRecv(1), lsize, PREC_MPI, k, 0, this%mpi_comm_cart, req(1), ierr )
	 I=0
	  do iz=zStart,zEnd
   	   do iy=yStart,yEnd
            do ix=xStart,xEnd
              I = I+1
              f_global_0(ix,iy,iz) = mRecv(I)
             enddo
            enddo
           enddo
	
    enddo

  else
      
       call MPI_SEND( this%xlft, 1, MPI_INTEGER,0, 1, this%mpi_comm_cart, ierr)
       call MPI_SEND( this%xrgt, 1, MPI_INTEGER,0, 2, this%mpi_comm_cart, ierr)
       call MPI_SEND( this%ylft, 1, MPI_INTEGER,0, 3, this%mpi_comm_cart, ierr)
       call MPI_SEND( this%yrgt, 1, MPI_INTEGER,0, 4, this%mpi_comm_cart, ierr)
       call MPI_SEND( this%zlft, 1, MPI_INTEGER,0, 5, this%mpi_comm_cart, ierr)
       call MPI_SEND( this%zrgt, 1, MPI_INTEGER,0, 6, this%mpi_comm_cart, ierr)
       call MPI_SEND( mSend(1), lsize, PREC_MPI,0, 0, this%mpi_comm_cart, ierr)	
  
  endif     

  deallocate(mSend)
  deallocate(mRecv)
  
  call MPI_BCAST(f_global_0,gsize,PREC_MPI,0,this%mpi_comm_cart, ierr)	
  f_global(:,:,:) = f_global_0(:,:,:)
  
  deallocate(f_global_0)
#endif
  return

  end subroutine updateGlobal_3d

 !-----------------------------------------------------------------

 subroutine copySendBuffer_3d(this, fieldSend, nmax, xStart, xEnd, yStart, yEnd, zStart, zEnd, f) 
   implicit none
   class(mpidomain) :: this 
   real(kind=PREC_F90), intent(out) :: fieldSend(1:nmax)
   real(kind=PREC_F90), intent(in)  :: f(this%xs:this%xe, &
                                         this%ys:this%ye, &
                                         this%zs:this%ze)
   integer, intent(in) :: nmax
   integer, intent(in) :: xStart, xEnd
   integer, intent(in) :: yStart, yEnd
   integer, intent(in) :: zStart, zEnd
   integer :: ix,iy,iz,I

   I=0
   do iz=zStart,zEnd
    do iy=yStart,yEnd
      do ix=xStart,xEnd
	I = I + 1		
	fieldSend(I) = f(ix,iy,iz)
      enddo
     enddo
   enddo
   
 end subroutine copySendBuffer_3d
!-----------------------------------------------------------------
subroutine copyRecvBuffer_3d(this, fieldRecv, nmax, xStart, xEnd, yStart, yEnd, zStart, zEnd, f) 
   implicit none
   class(mpidomain) :: this 
   real(kind=PREC_F90), intent(in)  :: fieldRecv(1:nmax)
   real(kind=PREC_F90), intent(out) :: f(this%xs:this%xe, &
                                         this%ys:this%ye, &
                                         this%zs:this%ze)
   integer, intent(in) :: nmax
   integer, intent(in) :: xStart, xEnd
   integer, intent(in) :: yStart, yEnd
   integer, intent(in) :: zStart, zEnd
   integer :: ix,iy,iz,I
   
   I=0
   do iz=zStart,zEnd
    do iy=yStart,yEnd
      do ix=xStart,xEnd
	I = I + 1		
	f(ix,iy,iz) = fieldRecv(I)
      enddo
     enddo
   enddo
   
 end subroutine copyRecvBuffer_3d

!-----------------------------------------------------------------

 subroutine alloc_4d(this,arr,ws,we,withhalos)

    class(mpidomain) :: this 
    real(kind=PREC_F90),allocatable :: arr(:,:,:,:)
    integer, intent(in) :: ws,we
    logical, intent(in) :: withhalos
    
    if(withhalos) then
      allocate (arr(ws:we,this%xs:this%xe,this%ys:this%ye,this%zs:this%ze))
    else
      allocate (arr(ws:we,this%xlft:this%xrgt,this%ylft:this%yrgt,this%zlft:this%zrgt))
    endif
    
#ifdef DEBUG_MPI
    print '(I3,A,(I3,A,I3),A,(I3,A,I3),A,(I3,A,I3),A,(I3,A,I3))',this%rank,' allocated array: ',&
         &lbound(arr,1),':',ubound(arr,1),',',&
         &lbound(arr,2),':',ubound(arr,2),',',&
         &lbound(arr,3),':',ubound(arr,3),',',&
         &lbound(arr,4),':',ubound(arr,4)
#endif

  end subroutine alloc_4d
!-----------------------------------------------------------------
  subroutine dealloc_4d(this,arr)

    class(mpidomain) :: this 
    real(kind=PREC_F90), allocatable :: arr(:,:,:,:)

#ifdef DEBUG_MPI
    call MPI_Barrier(comm%mpicomm, ierr)
    print '(I3,A,(I3,A,I3),A,(I3,A,I3),A,(I3,A,I3),A,(I3,A,I3))',this%rank,' deallocated array: ',&
         &lbound(arr,1),':',ubound(arr,1),',',&
         &lbound(arr,2),':',ubound(arr,2),',',&
         &lbound(arr,3),':',ubound(arr,3),',',&
         &lbound(arr,4),':',ubound(arr,4)
    call MPI_Barrier(comm%mpicomm, ierr)
#endif
    deallocate (arr)
    
    end subroutine dealloc_4d
!-----------------------------------------------------------------

  subroutine updateHalos_4darray(this,g,ws,we)

    implicit none

    class(mpidomain) :: this 
    integer, intent(in) :: ws,we
    real(kind=PREC_F90), intent(inout) :: g(ws:we, this%xs:this%xe, &
                                                   this%ys:this%ye, &
                                                   this%zs:this%ze)
#ifdef MPICODE
    real(kind=PREC_F90), allocatable :: fieldSend(:), fieldRecv(:)
    integer :: req
    integer :: status(MPI_STATUS_SIZE)
    integer :: disp, dir
    integer :: source, dest, tag
    integer :: xStart, xEnd
    integer :: yStart, yEnd
    integer :: zStart, zEnd
    integer :: ix,iy,iz,I,wsize
  
    wsize = we-ws+1
    allocate(fieldRecv(1:this%maxBufLen*wsize))
    allocate(fieldSend(1:this%maxBufLen*wsize))
     
    do disp = -1, 1, 2
     do dir = 1,domain_rank(this)
     
     call MPI_CART_SHIFT(this%mpi_comm_cart, (dir-1) , disp, source, dest, ierr)

#ifdef DEBUG_MPI  
     print '(A,I3,2X,A,I3,2X,A,I3,2X,A,I3,2X,A,I3,4X,8I3)',&
        'Rank: ',this%rank, 'dir-1: ',dir-1 , 'disp: ',disp, 'R From: ',source, 'S to: ', dest, &
	         this%sendmsg(disp,dir-1,1,1), this%sendmsg(disp,dir-1,2,1), &
	         this%sendmsg(disp,dir-1,1,2), this%sendmsg(disp,dir-1,2,2), &
	         this%recvmsg(disp,dir-1,1,1), this%recvmsg(disp,dir-1,2,1), &
	         this%recvmsg(disp,dir-1,1,2), this%recvmsg(disp,dir-1,2,2) 
#endif	         
     tag = (disp + 1) + dir   	
     if(source /= MPI_PROC_NULL) then
       call MPI_IRECV( fieldRecv(1), this%maxBufLen*wsize, PREC_MPI, &
                       source, tag, this%mpi_comm_cart, req, ierr )
     endif

     if(dest /= MPI_PROC_NULL) then
       
        xStart = this%sendmsg(disp,dir-1,1,0)
        xEnd   = this%sendmsg(disp,dir-1,2,0)
        yStart = this%sendmsg(disp,dir-1,1,1)
        yEnd   = this%sendmsg(disp,dir-1,2,1)
        zStart = this%sendmsg(disp,dir-1,1,2)
        zEnd   = this%sendmsg(disp,dir-1,2,2)	

       call this%copySendBuffer(fieldSend, this%maxBufLen*wsize, ws, we, xStart, xEnd, yStart ,yEnd, zStart, zEnd, g)
       	
       call MPI_SEND( fieldSend(1), this%maxBufLen*wsize, PREC_MPI, &
                      dest, tag, this%mpi_comm_cart, ierr)
     endif 

     if(source /= MPI_PROC_NULL) then
       call MPI_WAIT(req, status, ierr)
       
        !(disp,dir,start:end,x,y or z)
        xStart = this%recvmsg(disp,dir-1,1,0)
        xEnd   = this%recvmsg(disp,dir-1,2,0)
        yStart = this%recvmsg(disp,dir-1,1,1)
        yEnd   = this%recvmsg(disp,dir-1,2,1)
        zStart = this%recvmsg(disp,dir-1,1,2)
        zEnd   = this%recvmsg(disp,dir-1,2,2)

       call this%copyRecvBuffer(fieldRecv, this%maxBufLen*wsize, ws, we, xStart, xEnd, yStart, yEnd, zStart, zEnd, g)

     endif

     enddo
   enddo  
      
  deallocate(fieldSend)
  deallocate(fieldRecv)
#endif
  return
  end subroutine updateHalos_4darray  
  !-----------------------------------------------------------------
subroutine updateGlobal_4d(this,g,ws,we,g_global)
! USED ONLY for debugging
! called by all ranks to fill the global array (f_global) with
! values from a local array(g) on the local domain
!
! input:  g local  array (domain, without halos!)
! output: g_global global array (entire grid, with boundaries)
!
! note that this is an auxiliary routine for development only, not
! optimized for performance


    implicit none
    
    class(mpidomain) :: this 
    integer, intent(in) :: ws,we
    real(kind=PREC_F90), intent(in)  :: g(ws:we,this%xs:this%xe, &
                                                this%ys:this%ye, &
                                                this%zs:this%ze)    !local

    real(kind=PREC_F90), intent(inout) :: g_global(:,:,:,:) !global
#ifdef MPICODE
    real(kind=PREC_F90), allocatable :: g_global_0(:,:,:,:) !global to process 0
    
    integer :: lsize, gsize
    real(kind=PREC_F90),allocatable, dimension(:) :: mSend,mRecv
    integer :: ix,iy,iz,I,jj,k,iw
    integer :: xStart, xEnd
    integer :: yStart, yEnd
    integer :: zStart, zEnd
    integer :: req(7)

    lsize = (this%zrgt-this%zlft+1)*(this%yrgt-this%ylft+1)*(this%xrgt-this%xlft+1)*(we-ws+1)
    gsize = size(g_global,dim=1)*size(g_global,dim=2)*size(g_global,dim=3)*(we-ws+1)

    allocate(mSend(lsize))
    allocate(mRecv(lsize))
    
    allocate(g_global_0(ws:we, 1:size(g_global,dim=2),1:size(g_global,dim=3),1:size(g_global,dim=4)) )
    
    
    I=0
    do iz=this%zlft,this%zrgt
      do iy=this%ylft,this%yrgt
	do ix=this%xlft,this%xrgt
	  do iw=ws,we
            I = I+1
	    mSend(I) = g(iw,ix,iy,iz)
	  enddo
        enddo
      enddo
    enddo  


   if(this%mpirank() .eq. 0) then
    I=0 
    do iz=this%zlft,this%zrgt
      do iy=this%ylft,this%yrgt
	do ix=this%xlft,this%xrgt
	  do iw=ws,we
            I = I+1
            g_global_0(iw,ix,iy,iz) = mSend(I)
          enddo
        enddo
      enddo
    enddo
  
    do k=1,(this%mpiproc() -1)
        
      	call MPI_RECV( xStart, 1, MPI_INTEGER, k, 1, this%mpi_comm_cart, req(1), ierr )
	call MPI_RECV( xEnd,   1, MPI_INTEGER, k, 2, this%mpi_comm_cart, req(1), ierr )
	call MPI_RECV( yStart, 1, MPI_INTEGER, k, 3, this%mpi_comm_cart, req(1), ierr )
	call MPI_RECV( yEnd,   1, MPI_INTEGER, k, 4, this%mpi_comm_cart, req(1), ierr )
	call MPI_RECV( zStart, 1, MPI_INTEGER, k, 5, this%mpi_comm_cart, req(1), ierr )
	call MPI_RECV( zEnd,   1, MPI_INTEGER, k, 6, this%mpi_comm_cart, req(1), ierr )
        call MPI_RECV( mRecv(1), lsize, PREC_MPI, k, 0, this%mpi_comm_cart, req(1), ierr )
	 I=0
	  do iz=zStart,zEnd
   	   do iy=yStart,yEnd
            do ix=xStart,xEnd
              do iw=ws,we
                I = I+1
                g_global_0(iw,ix,iy,iz) = mRecv(I)
               enddo
              enddo
             enddo
           enddo
	
    enddo

  else
      
       call MPI_SEND( this%xlft, 1, MPI_INTEGER,0, 1, this%mpi_comm_cart, ierr)
       call MPI_SEND( this%xrgt, 1, MPI_INTEGER,0, 2, this%mpi_comm_cart, ierr)
       call MPI_SEND( this%ylft, 1, MPI_INTEGER,0, 3, this%mpi_comm_cart, ierr)
       call MPI_SEND( this%yrgt, 1, MPI_INTEGER,0, 4, this%mpi_comm_cart, ierr)
       call MPI_SEND( this%zlft, 1, MPI_INTEGER,0, 5, this%mpi_comm_cart, ierr)
       call MPI_SEND( this%zrgt, 1, MPI_INTEGER,0, 6, this%mpi_comm_cart, ierr)
       call MPI_SEND( mSend(1), lsize, PREC_MPI,0, 0, this%mpi_comm_cart, ierr)	
  
  endif     

  deallocate(mSend)
  deallocate(mRecv)
  
  call MPI_BCAST(g_global_0,gsize,PREC_MPI,0,this%mpi_comm_cart, ierr)	
  g_global(:,:,:,:) = g_global_0(:,:,:,:)
  
  deallocate(g_global_0)
#endif
  return
  end subroutine updateGlobal_4d
!-----------------------------------------------------------------
 subroutine copySendBuffer_4d(this, fieldSend, nmax, ws, we, xStart, xEnd, yStart, yEnd, zStart, zEnd, g) 
   implicit none
   class(mpidomain) :: this 
   integer, intent(in) :: ws, we
   real(kind=PREC_F90), intent(out) :: fieldSend(1:nmax)
   real(kind=PREC_F90), intent(in)  :: g(ws:we,this%xs:this%xe, &
                                               this%ys:this%ye, &
                                               this%zs:this%ze)
   integer, intent(in) :: nmax
   integer, intent(in) :: xStart, xEnd
   integer, intent(in) :: yStart, yEnd
   integer, intent(in) :: zStart, zEnd
   integer :: ix,iy,iz,ik,I,wsize
   
   wsize=we-ws+1
   I=0
   do iz=zStart,zEnd
    do iy=yStart,yEnd
      do ix=xStart,xEnd
        do ik=ws,we
	  I = I + 1		
	  fieldSend(I) = g(ik,ix,iy,iz)
	enddo
      enddo
     enddo
   enddo
   
 end subroutine copySendBuffer_4d
!-----------------------------------------------------------------
subroutine copyRecvBuffer_4d(this, fieldRecv, nmax, ws, we, xStart, xEnd, yStart, yEnd, zStart, zEnd, g) 
   implicit none
   class(mpidomain) :: this
   integer, intent(in) :: ws, we
   real(kind=PREC_F90), intent(in)  :: fieldRecv(1:nmax)
   real(kind=PREC_F90), intent(out) :: g(ws:we,this%xs:this%xe, &
                                               this%ys:this%ye, &
                                               this%zs:this%ze)
   integer, intent(in) :: nmax
   integer, intent(in) :: xStart, xEnd
   integer, intent(in) :: yStart, yEnd
   integer, intent(in) :: zStart, zEnd
   integer :: ix,iy,iz,ik,I,wsize
   
   wsize=we-ws+1
   I=0
   do iz=zStart,zEnd
    do iy=yStart,yEnd
      do ix=xStart,xEnd
        do ik=ws,we
	  I = I + 1		
	  g(ik,ix,iy,iz) = fieldRecv(I)
	enddo
      enddo
     enddo
   enddo
   
 end subroutine copyRecvBuffer_4d
!-----------------------------------------------------------------

  logical function isHalocell(this,i,j,k)
    implicit none

    class(mpidomain) :: this 
    integer, intent(in) :: i,j,k
    integer :: ii,jj
    
    isHalocell=.false.
   
    print *, '*** isHalocell: NOT Yet Implemented for 1D, 2D and 3D'
  end function isHalocell
!---------------------------
  integer function nhalo(this)
    implicit none

    class(mpidomain) :: this 

    nhalo=this%nh
    
  end function nhalo
!-----------------------------
  integer function xmin(this)
    implicit none

    class(mpidomain) :: this 
    
    if(this%nx > 2*this%nh) then
      xmin=this%xs+this%nh
    else 
      xmin=this%xs
    endif
     
  end function xmin
!-----------------------------
  integer function xmax(this)
    implicit none

    class(mpidomain) :: this 
    
    if(this%nx > 2*this%nh) then
      xmax=this%xe-this%nh
    else 
      xmax=this%xe
    endif
       
  end function xmax
  !-----------------------------
  integer function get_xs(this)
    implicit none

    class(mpidomain) :: this 
 
    get_xs=this%xs
  end function get_xs
!-----------------------------
  integer function get_xe(this)
    implicit none

    class(mpidomain) :: this 

    get_xe=this%xe
    
  end function get_xe
!-----------------------------
  integer function ymin(this)
    implicit none

    class(mpidomain) :: this 
    
    if(this%ny > 2*this%nh) then
      ymin=this%ys+this%nh
    else 
      ymin=this%ys
    endif
    
  end function ymin
!-----------------------------
  integer function ymax(this)
    implicit none

    class(mpidomain) :: this 

    if(this%ny > 2*this%nh) then
      ymax=this%ye-this%nh
    else 
      ymax=this%ye
    endif
    
  end function ymax
  !-----------------------------
  integer function get_ys(this)
    implicit none

    class(mpidomain) :: this 
    
    get_ys=this%ys
      
  end function get_ys
!-----------------------------
  integer function get_ye(this)
    implicit none

    class(mpidomain) :: this 
    
    get_ye=this%ye
        
  end function get_ye
!-----------------------------
   integer function zmin(this)
    implicit none

    class(mpidomain) :: this 
    
    if(this%nz > 2*this%nh) then
      zmin=this%zs+this%nh
    else 
      zmin=this%zs
    endif
  end function zmin
!-----------------------------
  integer function zmax(this)
    implicit none

    class(mpidomain) :: this 

    if(this%nz > 2*this%nh) then
      zmax=this%ze-this%nh
    else 
      zmax=this%ye
    endif
    
  end function zmax
!-----------------------------
  integer function get_zs(this)
    implicit none

    class(mpidomain) :: this 

    get_zs=this%zs
  end function get_zs
!-----------------------------
  integer function get_ze(this)
    implicit none

    class(mpidomain) :: this 

    get_ze=this%ze
    
  end function get_ze
!-----------------------------
  integer function get_lx(this)
    implicit none

    class(mpidomain) :: this 

    get_lx=this%nx
    
  end function get_lx

!-----------------------------
  integer function get_ly(this)
    implicit none

    class(mpidomain) :: this 

    get_ly=this%ny
    
  end function get_ly

!-----------------------------
  integer function get_lz(this)
    implicit none

    class(mpidomain) :: this 

    get_lz=this%nz
    
  end function get_lz
 !-----------------------------
  integer function get_gx(this)
    implicit none

    class(mpidomain) :: this 

    get_gx=this%gx
    
  end function get_gx

!-----------------------------
integer function get_gy(this)
    implicit none

    class(mpidomain) :: this 

    get_gy=this%gy
    
  end function get_gy

!-----------------------------
  integer function get_gz(this)
    implicit none

    class(mpidomain) :: this 

    get_gz=this%gz
    
  end function get_gz
  !-----------------------------
  integer function get_xlft(this)
    implicit none

    class(mpidomain) :: this 

    get_xlft=this%xlft
    
  end function get_xlft
 !-----------------------------
   integer function get_xrgt(this)
    implicit none

    class(mpidomain) :: this 

    get_xrgt=this%xrgt
    
  end function get_xrgt
 !-----------------------------
 integer function get_ylft(this)
    implicit none

    class(mpidomain) :: this 

    get_ylft=this%ylft
    
  end function get_ylft
 !-----------------------------
   integer function get_yrgt(this)
    implicit none

    class(mpidomain) :: this 

    get_yrgt=this%yrgt
    
  end function get_yrgt
 !-----------------------------
 integer function get_zlft(this)
    implicit none

    class(mpidomain) :: this 

    get_zlft=this%zlft
    
  end function get_zlft
 !-----------------------------
   integer function get_zrgt(this)
    implicit none

    class(mpidomain) :: this 

    get_zrgt=this%zrgt
    
  end function get_zrgt
 !-----------------------------
   integer function get_buflen(this)
    implicit none

    class(mpidomain) :: this

    get_buflen=this%maxBufLen

  end function get_buflen
 !-----------------------------
  integer function domain_rank(this)
    implicit none

    class(mpidomain) :: this 

    domain_rank=this%cart_dimens
    
  end function domain_rank


!-----------------------------
  integer function mpirank(this)
    implicit none

    class(mpidomain) :: this 

    mpirank=this%rank
    
  end function mpirank
  !-----------------------------
  integer function mpiproc(this)
    implicit none

    class(mpidomain) :: this 

    mpiproc=this%nproc
    
  end function mpiproc

!-----------------------------
  function mpicoord(this)
    implicit none

    class(mpidomain) :: this 
    integer :: mpicoord(3)

    mpicoord=this%coord
    
  end function mpicoord

!-----------------------------
  function mpiperiod(this)
    implicit none

    class(mpidomain) :: this 
    logical :: mpiperiod(3)

    mpiperiod=this%periodic
    
  end function mpiperiod
  
  !-----------------------------

end module mod_mpidomain

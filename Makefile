F90 = mpiifort
#mpiifort
F90DFLAGS = -g -traceback -check all -debug
F90FLAGS = -DDOUBLE_PREC -cpp -O3 -DMPICODE 
#-qopenmp 
#-DMPICODE

INCLUDES 	= -I$(HDF5_HOME)/include
LIBS 		=  
#$(HDF5_HOME)/lib/libhdf5_fortran.a $(HDF5_HOME)/lib/libhdf5.a -lz 
#-lgpfs

OBJS = mod_precision.o mod_mpi.o mod_mpidomain.o  test_domdec.o
#mod_hdf5io.o
EXE = test_domdec

%.o:%.f90
	$(F90) $(F90FLAGS) $(INCLUDES) -c $<

$(EXE): $(OBJS)
	$(F90) $(F90FLAGS) -o $@ $(OBJS) $(LIBS) 

.SUFFIXES: $(SUFFIXES) .f90

clean:
	rm -f *.o *.mod test_domdec

fclean:
	rm -f *.o *.mod *.h5 test_domdec


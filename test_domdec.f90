!-----------------------------------------------------------------
!
! Simple test program for the mpidomain class (1D-2D domain decomp)
! and hdf5 I/O
! authors: Markus Rampp, Fabio Baruffa (RZG) 
!-----------------------------------------------------------------

program main
use precision
#ifdef MPICODE
use mpi
#endif
use my_mpi
use mod_mpidomain
!use mod_hdf5io
implicit none

!problem setup
!integer, parameter :: xsize=64,ysize=64,zsize=64  ! global grid dimensions
integer :: xsize, ysize, zsize
integer, parameter :: gs=1                               ! number of ghost zones
integer, parameter :: nstep_max=100                      ! number of maximum time steps
integer, parameter :: stride=1000                          !Allreduce each 'stride' loop
integer, parameter :: domain_rank=2		                  ! rank of the domain decomposition

real(kind=PREC_F90), allocatable :: f(:,:,:), f0(:,:,:)  ! local array for each MPI rank
real(kind=PREC_F90), allocatable :: f_global(:,:,:)

double precision dy,dz, dt, t
double precision weighty, weightz, diagy, diagz
double precision local_diff, global_diff
double precision epsilon

integer :: sizes(4)
integer :: s_x, s_y, s_z
integer :: step
integer :: buffer_size
integer :: local_size

logical converged

integer :: reqs(8)
integer :: statuses(MPI_STATUS_SIZE,8)

double precision start_time, end_time
double precision comm_time, criterion_time, copy_time, comp_time

real(kind=PREC_F90) :: scale
real(kind=PREC_F90) :: arr_1d(10)
integer :: n,i,j,k

integer :: source, dest, rank
!MPI init
call initmpi(ierr)

if(comm%isRoot()) then
    print '(A5,A,A5,A,A5,A,A15,A,A15,A,A15,A,3X,A15,A,A15,A,A15,A,A15)', &
     'nx',',', 'ny',',', 'nz', ',','nx*ny*nz',',','local size',',','buf len',',', 'tot time(s)', ',','copy time(s)',',','comp time(s)',',', 'comm time(s)'
endif

sizes = (/64, 128, 256, 512/)
do s_x =1,4
!do s_y =s_x,4
!do s_z =s_y,4
s_y=s_x
s_z=s_x
xsize=sizes(s_x)
ysize=sizes(s_y)
zsize=sizes(s_z)

!domain init
domain=mpidomain(domain_rank,xsize,ysize,zsize,gs,ierr)

!produce some informational output for the domain
!call domain%infoDomain()
!call domain%info()

! allocate a 3D array with halo cells
!f(x,y,z) -> f(1,y,z) 
call domain%alloc(f ,withhalos=.true.)
call domain%alloc(f0,withhalos=.true.)

!allocate(f_global(1:xsize,1:ysize,1:zsize))

call MPI_CART_SHIFT(domain%mpi_comm_cart, 1 , 1, source, dest, ierr)
call MPI_COMM_RANK(domain%mpi_comm_cart,rank,ierr)
!write(*,*), rank, source, dest

epsilon=1.0e-8

dy=1.d0/dble(ysize)
dz=1.d0/dble(zsize)

dt = 0.25*(min(dy,dz)**2)

weighty = dt/(dy*dy)
weightz = dt/(dz*dz)
diagy = -2.0 + dy*dy/(2.0*dt)
diagz = -2.0 + dz*dz/(2.0*dt)

!if(comm%isRoot()) then
!   write (*,'(/,a)'), 'Heat Conduction 2d : START'
!   write (*,'(/,4(a,1pg12.4))'), 'dy =',dy,', dz =',dz,', dt =',dt,', eps =',epsilon
!   write (*,*)
!endif 

do k=domain%get_zs(),domain%get_ze()
   do j=domain%get_ys(),domain%get_ye()
      do i=domain%get_xs(),domain%get_xe()
      f0(i,j,k) = -10.0
    enddo
  enddo
enddo

if(domain%get_zs().eq.1) then
  do j=domain%get_ys(),domain%get_ye()
   f0(1,j,1)=10.0
  enddo
endif
if(domain%get_ze().eq.zsize) then
   do j=domain%get_ys(),domain%get_ye()
      f0(1,j,zsize) = 10.0
   enddo
endif

if(domain%get_ys().eq.1) then
  do k=domain%get_zs(),domain%get_ze()
     f0(1,1,k)=10.0
  enddo
endif
if(domain%get_ye().eq.ysize) then
   do k=domain%get_zs(),domain%get_ze()
      f0(1,ysize,k) = 10.0
   enddo
endif

call domain%exchangeHalos(f0, reqs, statuses)   	         !update halos
call MPI_WAITALL(8, reqs, statuses, ierr)
!call printFlocalf0

global_diff = 1.d0
local_diff  = 0.d0
start_time = MPI_WTIME()
comm_time = 0.d0
criterion_time = 0.d0
converged = .false.
copy_time = 0.d0

do step=1, nstep_max
  local_diff  = 0.d0
  
  copy_time = copy_time - MPI_WTIME()  
   call domain%copy2SendBuf(f0)
  copy_time = copy_time + MPI_WTIME()
 
  comm_time = comm_time - MPI_WTIME()
   call domain%exchangeHalos(f0, reqs, statuses)   	            !exchange the halos
  comm_time = comm_time + MPI_WTIME()

  call updateInnerField(local_diff)           	!update inner field (no halo needed)
  
  comm_time = comm_time - MPI_WTIME()
   call MPI_WAITALL(8, reqs, statuses, ierr)
  comm_time = comm_time + MPI_WTIME()

  copy_time = copy_time - MPI_WTIME()
   call domain%copy2RecvBuf(f0)
  copy_time = copy_time + MPI_WTIME()

  call updateHaloCells(local_diff)              !update the halo borders

!   criterion_time = criterion_time - MPI_WTIME() 
!     !call checkConv(step,stride,local_diff,converged)
!   criterion_time = criterion_time + MPI_WTIME() 
!   if(converged) then
!    exit
!   endif 
enddo
end_time = MPI_WTIME()

!call printFlocalf0
! call MPI_Barrier(MPI_COMM_WORLD, ierr)
! call domain%updateGlobal(f0,f_global)
! call MPI_Barrier(MPI_COMM_WORLD, ierr)
! call printFGlobal
! call MPI_Barrier(MPI_COMM_WORLD, ierr)

!if(comm%isRoot()) then
!   write (*,'(/,a)'), 'Heat Conduction 2d : STOP'
!   write (*,*), '  Number of step = ', step
!   write (*,*), '  Total size = ', xsize*ysize*zsize
!   write (*,*), '  Total time = ', end_time - start_time
!   write (*,*), '  Comm. time = ', comm_time
!   write (*,*), '  Crit. time = ', criterion_time
!endif
buffer_size = domain%get_buflen()
local_size = domain%get_lx()*domain%get_ly()*domain%get_lz()
comp_time=(end_time-start_time) - copy_time
if(comm%isRoot()) then
    print '(I5,A,I5,A,I5,A,I15,A,I15,A,I15,A,3X,E15.7,A,E15.7,A,E15.7,A,E15.7)', &
    xsize,",",ysize,",",zsize,",",xsize*ysize*zsize,",",local_size,",", buffer_size,",",end_time - start_time,",",copy_time,",",  comp_time, ",", comm_time
endif 

!cleanup
!deallocate(f_global)

call domain%dealloc(f)
call domain%dealloc(f0)

call domain%finalize()

enddo
!enddo
!enddo

call comm%finalize()

 contains

subroutine checkConv(step,stride,local_diff,converged)
  implicit none
  integer, intent(in) :: step, stride
  double precision, intent(in) :: local_diff
  logical, intent (inout) :: converged

  if (mod(step,stride) .eq. 0) then
      
   call MPI_Allreduce(local_diff, global_diff, 1, MPI_DOUBLE_PRECISION, MPI_SUM, comm, ierr )
   global_diff = sqrt(global_diff)
   
   endif
   if (global_diff.lt.epsilon)  then
      converged = .true.
   endif

end subroutine checkConv

subroutine updateInnerField(diff)
    implicit none

    double precision, intent(inout) :: diff
    double precision ldiff
    integer i,j,k

! !2D Heat Flow
!    do k=domain%get_zs()+gs,domain%get_ze()-gs
!        do j=domain%get_ys()+gs,domain%get_ye()-gs
!           do i=domain%get_xs(),domain%get_xe()
!             f(i,j,k) = weighty*(f0(i,j-1,k) + f0(i,j+1,k) + f0(i,j,k)*diagy) &
!                      + weightz*(f0(i,j,k-1) + f0(i,j,k+1) + f0(i,j,k)*diagz)
!           enddo
!        enddo
!     enddo

!    ! Compute the difference.
!     diff=0.0
!     do k=domain%get_zs()+gs,domain%get_ze()-gs
!       do j=domain%get_ys()+gs,domain%get_ye()-gs
!          do i=domain%get_xs(),domain%get_xe()
!             ldiff = f0(i,j,k) - f(i,j,k)
!             diff = diff + ldiff*ldiff
!             f0(i,j,k) = f(i,j,k)
!          end do
!       end do
!    enddo

     !2D Heat Flow

!$omp parallel do
  do k=domain%get_zlft()+gs,domain%get_zrgt()-gs
   do j=domain%get_ylft()+gs,domain%get_yrgt()-gs
      do i=domain%get_xlft(),domain%get_xrgt()
        f(i,j,k) = weighty*(f0(i,j-1,k) + f0(i,j+1,k) + f0(i,j,k)*diagy) &
                 + weightz*(f0(i,j,k-1) + f0(i,j,k+1) + f0(i,j,k)*diagz)
      enddo
   enddo
enddo

!Compute the difference.
!$omp parallel do 
do k=domain%get_zlft()+gs,domain%get_zrgt()-gs
  do j=domain%get_ylft()+gs,domain%get_yrgt()-gs
     do i=domain%get_xlft(),domain%get_xrgt()
        ldiff = f0(i,j,k) - f(i,j,k)
        diff = diff + ldiff*ldiff
        f0(i,j,k) = f(i,j,k)
     end do
  end do
enddo

end subroutine updateInnerField


subroutine updateHaloCells(diff)
   implicit none

   double precision, intent(inout) :: diff
   double precision ldiff
   integer i,j,k

   !2D Heat Flow
k=domain%get_zs()+gs
!$omp parallel do
do j=domain%get_ys()+gs,domain%get_ye()-gs
   do i=domain%get_xlft(),domain%get_xrgt()
     f(i,j,k) = weighty*(f0(i,j-1,k) + f0(i,j+1,k) + f0(i,j,k)*diagy) &
              + weightz*(f0(i,j,k-1) + f0(i,j,k+1) + f0(i,j,k)*diagz)
   enddo
enddo
k=domain%get_ze()-gs
!$omp parallel do
do j=domain%get_ys()+gs,domain%get_ye()-gs
   do i=domain%get_xlft(),domain%get_xrgt()
     f(i,j,k) = weighty*(f0(i,j-1,k) + f0(i,j+1,k) + f0(i,j,k)*diagy) &
              + weightz*(f0(i,j,k-1) + f0(i,j,k+1) + f0(i,j,k)*diagz)
   enddo
enddo
j=domain%get_ys()+gs
!$omp parallel do
do k=domain%get_zs()+gs,domain%get_ze()-gs
      do i=domain%get_xlft(),domain%get_xrgt()
        f(i,j,k) = weighty*(f0(i,j-1,k) + f0(i,j+1,k) + f0(i,j,k)*diagy) &
                 + weightz*(f0(i,j,k-1) + f0(i,j,k+1) + f0(i,j,k)*diagz)
      enddo
enddo
j=domain%get_ye()-gs
!$omp parallel do
do k=domain%get_zs()+gs,domain%get_ze()-gs
      do i=domain%get_xlft(),domain%get_xrgt()
        f(i,j,k) = weighty*(f0(i,j-1,k) + f0(i,j+1,k) + f0(i,j,k)*diagy) &
                 + weightz*(f0(i,j,k-1) + f0(i,j,k+1) + f0(i,j,k)*diagz)
      enddo
enddo

! Compute the difference.
!diff=0.0
k=domain%get_zs()+gs
!$omp parallel do
do j=domain%get_ys()+gs,domain%get_ye()-gs
   do i=domain%get_xlft(),domain%get_xrgt()
      ldiff = f0(i,j,k) - f(i,j,k)
      diff = diff + ldiff*ldiff
      f0(i,j,k) = f(i,j,k)
   enddo
enddo
k=domain%get_ze()-gs
!$omp parallel do
do j=domain%get_ys()+gs,domain%get_ye()-gs
   do i=domain%get_xlft(),domain%get_xrgt()
      ldiff = f0(i,j,k) - f(i,j,k)
      diff = diff + ldiff*ldiff
      f0(i,j,k) = f(i,j,k)
   enddo
enddo
j=domain%get_ys()+gs
!$omp parallel do
do k=domain%get_zs()+gs,domain%get_ze()-gs
      do i=domain%get_xlft(),domain%get_xrgt()
         ldiff = f0(i,j,k) - f(i,j,k)
         diff = diff + ldiff*ldiff
         f0(i,j,k) = f(i,j,k)
      enddo
enddo
j=domain%get_ye()-gs
!$omp parallel do
do k=domain%get_zs()+gs,domain%get_ze()-gs
      do i=domain%get_xlft(),domain%get_xrgt()
         ldiff = f0(i,j,k) - f(i,j,k)
         diff = diff + ldiff*ldiff
         f0(i,j,k) = f(i,j,k)
      enddo
enddo

end subroutine updateHaloCells

subroutine printFlocalf0
    implicit none

    integer i,j,k,h
    
    if(comm%isRoot()) then
    do k=domain%get_zs(),domain%get_ze()
       do j=domain%get_ys(),domain%get_ye()
          do i=domain%get_xs(),domain%get_xe()
           print '(I3,A3,I3,A1,I3,A1,I3,A4,F10.5)',&
                 domain%mpirank(),'f0(',i,',',j,',',k,') = ',f0(i,j,k) 
        enddo
      enddo
    enddo
    endif
    
 end subroutine printFlocalf0

 subroutine printFlocalf
   implicit none

   integer i,j,k,h
   
   if(comm%isRoot()) then
   do k=domain%get_zs(),domain%get_ze()
      do j=domain%get_ys(),domain%get_ye()
         do i=domain%get_xs(),domain%get_xe()
          print '(I3,A2,I3,A1,I3,A1,I3,A4,F10.5)',&
                domain%mpirank(),'f(',i,',',j,',',k,') = ',f(i,j,k) 
       enddo
     enddo
   enddo
   endif
   
end subroutine printFlocalf

subroutine printFGlobal
   implicit none

   integer i,j,k,h
   
   if(comm%isRoot()) then
   do k=1,zsize
      do j=1,ysize
         do i=1,xsize
          print '(I3,A2,I3,A1,I3,A1,I3,A4,F10.5)',&
                domain%mpirank(),'f(',i,',',j,',',k,') = ',f_global(i,j,k) 
       enddo
     enddo
   enddo
   endif
   
end subroutine printFGlobal

end program main

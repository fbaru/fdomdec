!>
!> \par This module provides the real-kind, integer-kind definitions that are used in VERTEX
!>
!> \param rk    double precision floating point
!> \param rk4   single precision floating point
!> \param ik    the system integer
!>
module precision
  use iso_c_binding, only : C_FLOAT, C_DOUBLE, C_INT32_T, C_INT64_T
#ifdef MPICODE
  use mpi
#endif
  implicit none
      
  integer, parameter :: rk  = C_DOUBLE
  integer, parameter :: rk4 = C_FLOAT
#ifdef DOUBLE_PREC
  integer, parameter :: PREC_F90 = rk
#else
  integer, parameter :: PREC_F90 = rk4
#endif

#ifdef MPICODE
  integer, parameter :: PREC_MPI_RK  = MPI_DOUBLE_PRECISION
  integer, parameter :: PREC_MPI_RK4 = MPI_REAL
#ifdef DOUBLE_PREC
  integer, parameter :: PREC_MPI = PREC_MPI_RK
#else
  integer, parameter :: PREC_MPI = PREC_MPI_RK4
#endif
#endif

  integer, parameter :: ik  = C_INT64_T
  integer, parameter :: ik4 = C_INT32_T

end module precision

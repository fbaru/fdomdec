
module grid
  implicit none
  integer, parameter :: xmax=16, ymax=20, zmax=20,tmax=20000
  double precision :: eps = 1.d-08
  double precision :: dz, dy, dx, dz2, dy2, dx2, dz2i, dy2i, dx2i
  integer :: xstart = 1				!Starting point of the indexing
  integer :: ystart = 1	 
  integer :: zstart = 1				
  integer :: xsize, ysize, zsize		!Array sizes
  double precision, dimension(:,:,:), allocatable :: phi, phin
end module grid

module cart_mpi
  implicit none
  integer :: comm_cart
  integer, parameter :: cartDim = 2		!Dimensionality of the cartesian grid
  integer :: b1 = 1				!Number of ghosts cell
  integer :: zdim, ydim, xdim			!Dimensions of the cartesian grid
  integer :: dimsCart(3)			!Dimensions of the cartesian grid
  integer :: coords(3)				!Local coordinates
  logical :: periods(3), reorder
  integer :: left, right, lower, upper
  integer :: vertical_border, horizontal_border 
  integer :: zcoord, ycoord, xcoord		!Coordinates of the cartesian grid
  integer :: zinner, yinner, xinner		!Width of each interval
  integer :: zinner1, yinner1, xinner1	        !Auxiliary variables
  integer :: zouter, youter, xouter		!Outer dimension of each interval
  integer :: zs, ze, ys, ye, xs, xe		!Start and end local index
  integer :: zin, yin, xin			!Number of intervals in each dimension
end module cart_mpi

module mpi_var
  implicit none
  integer :: ierror
  integer :: size, rank
end module mpi_var

program main
  
  use grid
  use mpi
  use mpi_var
  use cart_mpi

  implicit none

  integer :: x,y,z,it,stride,i
  logical :: myerror
  logical :: converged
  double precision :: start_time, end_time
  double precision :: comm_time, criterion_time  
  double precision :: dphimax

  integer :: req(8), statuses(MPI_STATUS_SIZE,8)

  myerror = .false.
  converged = .false.
  stride = 100
  criterion_time = 0  
  start_time = 0
  comm_time = 0

  call MPI_INIT(ierror)
  call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierror)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, size, ierror)

	call distributeCart(myerror)
	
!Allocate local arrays
  allocate(phi (xs:xe,ys:ye,zs:ze))
  allocate(phin(xs+b1:xe-b1,ys+b1:ye-b1,zs:ze))

  if(rank==0) then
    write (*,'(/,a)'), 'Heat Conduction 2d : START'
    write (*,*)
  endif
  call MPI_BARRIER(MPI_COMM_WORLD,ierror)
  do i=0,size-1
    if(i.eq.0 .and. rank.eq.0) then 
       print '(11X, A13)','(  x  y  z )'
    endif
    if(rank.eq.i) then 
       print '(A7,I5,A1,3I3,A2,10(A5,I4,1X),A9,1F6.3,A,6I3)',&
            &'Rank:',rank,'(',coords,')',&
            &'xs:',xs, 'xe:',xe,&
            &'ys:',ys, 'ye:',ye,&
            &'zs:',zs, 'ze:',ze,&
            &'nx:',xe-xs+1, 'ny:',ye-ys+1,'nz:',ze-zs+1,'nh',b1
    endif
  enddo

	call initialize
	call createDatatype
		
	start_time = MPI_WTIME()
  do it=1,tmax

    comm_time = comm_time - MPI_WTIME()
     call exchangeGhosts(req, statuses, ierror)
    comm_time = comm_time + MPI_WTIME()	

		call integrateInner(dphimax)
    
    call integrateHalos(dphimax)
    comm_time = comm_time - MPI_WTIME()
     call MPI_WAITALL(8, req, statuses, ierror)
    comm_time = comm_time + MPI_WTIME()	

    criterion_time = criterion_time - MPI_WTIME()
      call checkConv(it,stride,dphimax,converged)
    criterion_time = criterion_time + MPI_WTIME()
    if(converged) then
      exit
    endif

	enddo
	end_time = MPI_WTIME()
	
	if(rank==0) then 
    	  do y = ys, ye
           do x = xs, xe
             write(*,*),x,y, phi(x, y,1)
           enddo
          enddo
        endif
	 
  if(rank==0) then
    write (*,'(/,a)'), 'Heat Conduction 2d : STOP'
    write (*,*), '  Number of step = ', it
    write (*,*), '  Total size = ', xsize*ysize*zsize
    write (*,*), '  Total time = ', end_time - start_time
    write (*,*), '  Comm. time = ', comm_time
    write (*,*), '  Crit. time = ', criterion_time
  endif 

  deallocate(phi)
  deallocate(phin)

  call MPI_FINALIZE(ierror)

end program 	

subroutine distributeCart(myerror)
  use grid
  use mpi
  use cart_mpi
  use mpi_var
  implicit none

  logical :: myerror

  xsize = (1 + xmax - xstart) 
  ysize = (1 + ymax - ystart) 
  zsize = (1 + zmax - zstart) 
      
  xdim = int(sqrt(real(size)))
  ydim = int(sqrt(real(size)))
  zdim = zmax

! Test if size is   1, 4, 9, 16, 25, ... or 2, 8, 18, 32, 50, ...
  if((xdim*ydim).ne.(size)) then
  	xdim = int(sqrt(real(size/2)))
        ydim = 2*xdim
  end if

  if((xdim*ydim).ne.(size)) then
     if(rank.eq.0) then
            print *, 'Number of nodes must be n*n or 2*n*n with n>0'
     end if
     myerror = .true.
  end if

  if( ( (ysize - 2*b1) .lt. ydim)  .or. &
    ( (xsize - 2*b1) .lt. xdim) ) then
     if(rank==0) then
	print *, 'Do not use more than', (zsize - 2*b1)*(ysize - 2*b1), ' nodes to compute this application!'
     endif
	myerror = .true.
   endif

!Creation of the 2D cartesian grid
	
  dimsCart(1) = xdim
  dimsCart(2) = ydim
  dimsCart(3) = zdim
  	
  periods(1) = .false.
  periods(2) = .false.
  periods(3) = .false.
  	
  reorder = .false.

  call MPI_CART_CREATE(MPI_COMM_WORLD, cartDim, dimsCart, periods, reorder, comm_cart, ierror)
  call MPI_COMM_RANK(comm_cart, rank, ierror)
  call MPI_CART_COORDS(comm_cart, rank, cartDim ,coords, ierror)

!local coordinates 	
  xcoord = coords(1)
  ycoord = coords(2)
  zcoord = 1

! whole z indecees   |------------- zsize = 1+zmax-zstart ------------|
!    start/end index  ^-zstart                                  zmax-^
! 
! 1. interval        |--- zouter1---|   
!                    |--|--------|--|
!                     b1  zinner1 b1 
!    start/end index  ^-zs      ze-^ 
! 2. interval                 |--|--------|--|   
! 3. interval                          |--|--------|--| 
! 4. interval                                   |--|-------|--| 
!                                                   zinner0 
! 5. interval = dimsCart's interval                     |--|-------|--|
!
! In each iteration on each interval, the inner area is computed
! by using the values of the last iteration in the whole outer area. 
! 
! zcoord = number of intervals - 1
!
! To fit exactly into isize, we use zin1 intervals of width zinner1
! and (dim-zin) intervals of width (zinner1 - 1) 
!
! zsize <= 2*b1 + zdim * zinner1  
!
! ==>   zinner1 >= (zsize - 2*b1) / dim
! ==>   smallest valid integer value:
  yinner1 = (ysize - 2*b1 - 1)/ ydim + 1
  yin     =  ysize - 2*b1 - ydim*(yinner1 - 1)
  if(ycoord .lt. yin) then
	yinner = yinner1
	ys = ystart + ycoord*yinner
  else
	yinner = yinner1 - 1
	ys = ystart + yin*yinner1 + (ycoord - yin) *yinner
  endif
  youter = yinner + 2*b1
  ye = ys + youter - 1	
!The same for x coordinate
  xinner1 = (xsize - 2*b1 - 1)/ xdim + 1
  xin     =  xsize - 2*b1 - xdim*(xinner1 - 1)
  if(xcoord .lt. xin) then
	xinner = xinner1
	xs = xstart + xcoord*xinner
  else
	xinner = xinner1 - 1
	xs = xstart + xin*xinner1 + (xcoord - xin) *xinner
  endif
  xouter = xinner + 2*b1
  xe = xs + xouter - 1	

  zs = zstart
  ze = zmax	
		
!       write(*,*) 'PE = ', rank,' zs = ', zs, ' ze = ', ze
!	write(*,*) 'PE = ', rank, ' zin = ', zin, ' of dim = ', zinner
!	write(*,*) 'PE = ', rank, ' zouter = ', zouter
   
end subroutine distributeCart

subroutine initialize
  use grid
  use mpi
  use cart_mpi
  use mpi_var
  implicit none

  integer :: z,y,x

  dz = 1.0/zmax
  dy = 1.0/ymax
  dx = 1.0/xmax

  dz2 = dz*dz
  dy2 = dy*dy
  dx2 = dx*dx

  dz2i = 1.0/dz2
  dy2i = 1.0/dy2
  dx2i = 1.0/dx2

!Start value 0.0
  do y = ys, min(ye,ymax-1)
    do x = max(1,xs), min(xe,xmax-1)
         phi(x,y,:) = 0.0
    enddo
  enddo

!Boundary conditions
  if(ye .eq. ymax) then
    do x = xs, xe
        phi(x,ymax,:) = 1.0
    enddo
  endif
!Start value dy
  if(xs .eq. 0) then
     phi(0, ys,:) = ys*dy
     do y = ys+1, min(ye,ymax-1)
        phi(0, y,:) = phi(0, y-1,:) + dy
    enddo
  endif

  if(xe .eq. xmax) then
     phi(xmax, ys,:) = ys*dy
    do y = ys+1, min(ye,ymax-1)
       phi(xmax, y,:) = phi(xmax, y-1,:) + dy
    enddo
   endif
  
!  if(rank==0) then 
!    do z = zs, ze
!     do y = ys, ye
!       write(*,*), y,z, phi(1, y, z)
!     enddo
!    enddo
!   endif

  return
end subroutine initialize

subroutine integrateInner(dphimax)

  use grid
  use mpi
  use cart_mpi
  use mpi_var
  implicit none
  
  integer :: stride
  double precision :: dphimax, dphi, dt, dphimaxpartial
  integer :: z,y,x
  integer :: b2

  dt = min(dy*dy,dx*dx)/4.0
  b2 = b1 + 1 
  dphimax = 0.0
   do z = zs, ze
    do y = ys+b2, ye-b2
      do x = xs+b2, xe-b2
        dphi = (phi(x+1,y,z) + phi(x-1,y,z) - 2.* phi(x,y,z) )*dx2i + &
               (phi(x,y+1,z) + phi(x,y-1,z) - 2.* phi(x,y,z) )*dy2i
        dphi = dphi * dt
        dphimax = max(dphimax,dphi)
        phin(x,y,z) = phi(x,y,z) + dphi
       enddo
     enddo
   enddo
  end subroutine integrateInner

  subroutine integrateHalos(dphimax)

    use grid
    use mpi
    use cart_mpi
    use mpi_var
    implicit none

    integer :: stride
    double precision :: dphimax, dphi, dt, dphimaxpartial
    integer :: z,y,x
    integer :: b2
  
    dt = min(dy*dy,dx*dx)/4.0
    b2 = b1 + 1 
    dphimax = 0.0
    
   do z = zs, ze
      y = ys+b1
      do x = xs+b1, xe-b1
        dphi = (phi(x+1,y,z) + phi(x-1,y,z) - 2.* phi(x,y,z) )*dx2i + &
               (phi(x,y+1,z) + phi(x,y-1,z) - 2.* phi(x,y,z) )*dy2i
        dphi = dphi * dt
        dphimax = max(dphimax,dphi)
        phin(x,y,z) = phi(x,y,z) + dphi
       enddo
      y = ye-b1
      do x = xs+b1, xe-b1
        dphi = (phi(x+1,y,z) + phi(x-1,y,z) - 2.* phi(x,y,z) )*dx2i + &
               (phi(x,y+1,z) + phi(x,y-1,z) - 2.* phi(x,y,z) )*dy2i
        dphi = dphi * dt
        dphimax = max(dphimax,dphi)
        phin(x,y,z) = phi(x,y,z) + dphi
       enddo
   enddo
   do z = zs, ze
    do y = ys+b1, ye-b1
        x = xs+b1
        dphi = (phi(x+1,y,z) + phi(x-1,y,z) - 2.* phi(x,y,z) )*dx2i + &
               (phi(x,y+1,z) + phi(x,y-1,z) - 2.* phi(x,y,z) )*dy2i
        dphi = dphi * dt
        dphimax = max(dphimax,dphi)
        phin(x,y,z) = phi(x,y,z) + dphi
     enddo
   enddo
   do z = zs, ze
    do y = ys+b1, ye-b1
        x = xe-b1
        dphi = (phi(x+1,y,z) + phi(x-1,y,z) - 2.* phi(x,y,z) )*dx2i + &
               (phi(x,y+1,z) + phi(x,y-1,z) - 2.* phi(x,y,z) )*dy2i
        dphi = dphi * dt
        dphimax = max(dphimax,dphi)
        phin(x,y,z) = phi(x,y,z) + dphi
     enddo
   enddo


  do y=ys+b1,ye-b1 
    do x=xs+b1,xe-b1
      phi(x,y,:)=phin(x,y,:)
    enddo
  enddo

  return
end subroutine integrateHalos


subroutine checkConv(it, stride, dphimax,converged)
  use grid
  use mpi
  use cart_mpi
  use mpi_var
  implicit none

  integer :: it, stride
  logical :: converged
  double precision :: dphimax, dphimaxpartial
!For optimization: allreduce only each stride's loop:
    if (mod(it,stride) .eq. 0) then 
      if (size .gt. 1) then 
        dphimaxpartial = dphimax
        call MPI_ALLREDUCE(dphimaxpartial, dphimax, 1, MPI_DOUBLE_PRECISION, MPI_MAX, comm_cart, ierror)
      endif 
    if(dphimax.lt.eps) converged = .true.
    endif 
end subroutine checkConv

subroutine createDatatype

  use grid
  use mpi
  use cart_mpi
  implicit none
  
  integer :: ierror

  call MPI_CART_SHIFT(comm_cart, 0, 1, left, right, ierror)
  call MPI_CART_SHIFT(comm_cart, 1, 1, lower, upper, ierror)
  
!Create a MPI Vector of count=b1, blocklegth=xinner, stride=xouter for the x horizontal border
  call MPI_TYPE_VECTOR(b1, xinner, xouter, MPI_DOUBLE_PRECISION, horizontal_border,ierror)
  call MPI_TYPE_COMMIT(horizontal_border, ierror)

!Create a MPI Vector of count=yinner, blocklegth=b1, stride=xouter for the y vertical border
  call MPI_TYPE_VECTOR(yinner, b1, xouter, MPI_DOUBLE_PRECISION, vertical_border,ierror)
  call MPI_TYPE_COMMIT(vertical_border, ierror)

  return
end subroutine createDatatype


subroutine exchangeGhosts(req, statuses, ierror)
  use grid
  use mpi
  use cart_mpi
  implicit none
  integer :: req(8), statuses(MPI_STATUS_SIZE,8), ierror

!send and receive to/from upper/lower
  call MPI_IRECV( phi(xs+b1, ys,1), 1, horizontal_border, lower, MPI_ANY_TAG, comm_cart, req(1), ierror)

  call MPI_IRECV( phi(xs+b1, ye,1), 1, horizontal_border, upper, MPI_ANY_TAG, comm_cart, req(2),ierror)

  call MPI_ISEND( phi(xs+b1, ye-b1,1), 1, horizontal_border, upper, 0, comm_cart, req(3), ierror)
  
  call MPI_ISEND( phi(xs+b1, ys+b1,1), 1, horizontal_border, lower, 0, comm_cart, req(4), ierror)
            
!  call MPI_WAITALL(4, req, statuses, ierror)

! send and receive to/from left/right

  call MPI_IRECV( phi(xs, ys+b1,1), 1, vertical_border, left,  MPI_ANY_TAG, comm_cart, req(5), ierror)

  call MPI_IRECV( phi(xe, ys+b1,1), 1, vertical_border, right, MPI_ANY_TAG, comm_cart, req(6), ierror)

  call MPI_ISEND( phi(xe-b1, ys+b1,1), 1, vertical_border, right, 0, comm_cart, req(7), ierror)
  
  call MPI_ISEND( phi(xs+b1, ys+b1,1), 1, vertical_border, left , 0, comm_cart, req(8), ierror)
            
  !call MPI_WAITALL(8, req, statuses, ierror)

  return
end subroutine exchangeGhosts







